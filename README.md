# Aloha Frontend
This react application is the frontend for the Aloha Project. All dependencies can be found in the package.json file

## Setup and Running Application

### Installing Dependencies
- Install [Node] on your machine (Use version 10.13.0 or higher)
- run `npm install` from inside the project folder

### Running App
- Make sure backend server is already running
- Confirm that mongo is running
- From inside the project folder, run `npm start` to run the development server

###
- The first time after system is deployed, go to `[URL]/install` to initiate the database collections.

### Set backend address
- Check the file src/configuration/config.js and set the correct address on which the backend is listening
## Features not yet implemented
- When project is created the form doesn't close automatically after submitting and the page has to be refreshed to show changes
- There is nothing preventing a card from being moved before it finishes its current stage
- Constraints have not been added to the create project form
- Project status page does not exist (popup when you click on card)




[Node]: https://nodejs.org/en/download/
