/*
export const ALOHA_FRONTEND_CONFIGURATION = {
    orchestratorAPIUrl: 'http://aloha-h2020.cloudapp.net:8080/',
    tensorboardUrl: 'http://aloha-h2020.cloudapp.net:8080/',
};

export const ALOHA_FRONTEND_CONFIGURATION = {
    orchestratorAPIUrl: 'http://localhost:5000/',
    tensorboardUrl: 'http://localhost:5000/',
};
export const ALOHA_FRONTEND_CONFIGURATION = {
    orchestratorAPIUrl: 'http://10.131.32.10:8080/',
    tensorboardUrl: 'http://10.131.32.10:8080/',
};

*/

export const ALOHA_FRONTEND_CONFIGURATION = {
    orchestratorAPIUrl: 'http://localhost:8080/',
    tensorboardUrl: 'http://localhost:8888/',
    netronUrl: 'http://localhost:8000/',
    version: '0.9.4'
};
