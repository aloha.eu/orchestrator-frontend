import React, { Component } from 'react';

class Tutorial extends Component {

  render(){
    return(
      <div>
        <div className="col-md-9 col-lg-10 px-4" data-spy="scroll" data-target="#navbar-tutorial" data-offset="0">
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mt-4 mb-2">
                <h1 className="h1">Getting Started</h1>
          </div>
          <div className="border-bottom">
            <p className="lead">The general purpose of the ALOHA ToolFlow is to provide a user-friendly environment to train Machine Learning algorithms for deploying them in a distributed architecture. </p>
          </div>
          <div className="pt-4" id="principles">
            <h3 className="h3">The principles of ALOHA</h3>
            <p><a href="http://aloha-h2020.eu" target="_blank" rel="noopener noreferrer">ALOHA EU H2020</a> project was meant in a state of the art where the universe of algorithm for machine learning is massive. This situation allows to use multiple methods, usually deployed into centralized servers in charge of the workload.</p>
            <p>At the same time, we are exploring the possibility of changing this paradigm of cloud processing, to a more locally-based and low-consumption deployment by using embedded systems as a workload force. This opportunity of processing into local devices, not only brings a reduction of latency, but also can reduce the power consumption and increase privacy.</p>
            <p>However, because there is a huge amount of possible architectures to be used, this type deployment requires specific skills and, assumes a time-consumption activity that not all organizations can afford.</p>
            <p>For that reason, the benefit of using ALOHA toolflow is that provides a simple way to design and train algorithms with specific settings for embedded architectures through a simple interface.</p>
            <p>Consequently, the time spent and the costs related to deploy and test Machine Learning algorithms can be radically reduced.</p>
            <p>The ALOHA Toolflow is industry agnostic, however it has been tested and optimized for the following applications:</p>
            <ul>
              <li>Cost and power-effective medical decision assistants</li>
              <li>Command recognition for smart industry applications</li>
              <li>Surveillance of critical infrastructure</li>
            </ul>
          </div>

          <div className="pt-4" id="installation">
            <h3 className="h3">Installation</h3>
            <p>You can follow installation steps from the Github Readme.</p>
          </div>

          <div className="pt-4" id="overview">
            <h3 className="h3">Overview</h3>
            <p>You can follow installation steps from the Github Readme.</p>
          </div>

          <div className="alert alert-warning">Work in progres...</div>
        </div>
        <nav className="col-md-2 bg-light sidebar sidebar-right" id="navbar-tutorial">
          <div className="sidebar-sticky">
          <h6 className="d-flex px-3 mt-4 mb-1 navbar-brand">
          <span>Documentation</span>
          </h6>
          <ul className="nav nav-pills flex-column">
            <li className="nav-item"><a href="#principles" className="nav-link">Principles</a></li>
            <li className="nav-item"><a href="#installation" className="nav-link">Installation</a></li>
            <li className="nav-item"><a href="#overview" className="nav-link">Overview</a></li>
          </ul>
          </div>
        </nav>
      </div>
    )
  }
}
export default Tutorial
