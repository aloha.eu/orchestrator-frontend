import React, {Component} from 'react'
import {RestService} from "../service/restService";

class DeleteProject extends Component{
  constructor(props){
    super(props)
  }

  handleClickDelete(cardId,laneId, projectId) { //deletes card and project
     console.log("Delete card and project");
    this.props.deleteProject({projectId: projectId, cardId: cardId, laneId: laneId})
  }
  render(){
    return(
      <div style={{maxWidth: "230px"}} className="text-wrap">
        <div className="control-group">
          {this.props.label === "Running" ? "You cannot delete a project while is running." : (
          <div>
            <p><strong>Are you sure that you want to delete the project {this.props.title} ?</strong><br />
            All the contents will be deleted permanently.</p>
            <div>
            <button onClick={ () => this.props.closeTip()} className="btn btn-sm btn-default mr-2">Cancel</button>
            <button onClick={ () => this.handleClickDelete(this.props.id, this.props.laneId, this.props.projectId)} className="btn btn-sm btn-danger">Delete</button>
            </div>
          </div>
          ) }

        </div>

      </div>
    )
  }

}
export default DeleteProject
