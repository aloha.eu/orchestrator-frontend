import React, {Component} from 'react'
import {RestService} from "../service/restService";

class CodeGen extends Component{
  constructor(props){
    super(props)
    this.state = {engines: ["NEURAgheGP", "NEURAgheHWCE", "espam"]}
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  handleSubmit(event){

  }

  engines(){
  return this.state.engines.map( (e, k) => {
      return(
        <div className="row toggle-group form-group mb-2" key={k}>
          <label className="col">{e.toUpperCase()}</label>
          <div className="col">
          {this.props.proj.code ?
            ( Object.keys(this.props.proj.code).length > 0 ?
            (
              this.props.proj.code[e] && this.props.proj.code[e]['status'] === 'Finished' ?
          <button onClick={() => RestService.getProjectFile(this.props.proj.code[e].path)} className="btn btn-sm btn-light">Download</button>
          :
          <button className="btn btn-sm btn-light" onClick={() => {RestService.codeProjectExecution(this.props.proj.id, e)}}>{this.props.proj.code[e] ? this.props.proj.code[e].status : 'Generate' }</button>
          ) : <button className="btn btn-sm btn-light" onClick={() => {RestService.codeProjectExecution(this.props.proj.id, e)}}>Generate</button>
          ) : <button className="btn btn-sm btn-light" onClick={() => {RestService.codeProjectExecution(this.props.proj.id, e)}}>Generate</button>
          }
          </div>
        </div>
      )
    })

  }
  render(){
    return(
      <div>
        <div className="control-group">
          {this.engines()}
        </div>
      { /*  <div className="control-group">
          <button className="btn btn-sm btn-primary" disabled={this.props.proj.model !== null ? false: true}
            onClick={() => this.handleClick2(this.props.id, this.props.laneId, this.props.projectId)}>{this.props.label === "Running" ? 'Stop' : 'Start'}
            </button>
        </div>
        */
      }
        {this.props.label === "Codegen_Finished"? <button onClick={() => RestService.getProjectFile(`experiments/prj_${this.proj.id}/code/onnx2c/${this.proj.title.split(' ').join('_')}.cpp`)} className={"btn btn-sm btn-primary"}>Download Code</button> : null}
      </div>
    )
  }

}
export default CodeGen
