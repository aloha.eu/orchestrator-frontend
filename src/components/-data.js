// mockup of what data looks like coming in from backend
export const data ={
    lanes: [
        {
            id: 'lane1',
            title: 'Planned Tasks',
            cards: [
                {id: 'Card1', title: 'Write Blog', description: 'Can AI make memes', label: '30 mins'},
                {id: 'Card2', title: 'Pay Rent', description: 'Transfer via NEFT', label: '5 mins', metadata: {sha: 'be312a1'}}
            ]
        },
        {
            id: 'lane2',
            title: 'Completed',
            cards: []
        }
    ]
};

const data_dummy ={
    lanes: [
        {
            id: 'lane1',
            title: 'Planned Tasks',
            cards: [
                {
                    id: 'Card1',
                    title: 'Write Blog',
                    description: 'Can AI make memes',
                    label: '30 mins',
                    tags: [
                      {title: 'Critical', color: 'white', bgcolor: 'red'},
                      {title: '2d ETA', color: 'white', bgcolor: '#0079BF'}
                    ]
                },
                {
                    id: 'Card2',
                    title: 'Pay Rent',
                    description: 'Transfer via NEFT',
                    label: '5 mins',
                    metadata: {sha: 'be312a1'}
                }
            ]
        },
        {
            id: 'lane2',
            title: 'Completed',
            cards: []
        }
    ]
}
