import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"

class EditPreset extends Component{
  constructor(props) {
      super(props);
      this.state = { message: '',
                    status: ''}
      this.handleSubmit = this.handleSubmit.bind(this);
      //this.handleChange = this.handleChange.bind(this);
      this.handleToggle = this.handleToggle.bind(this);
  }
  placeGroups(){
    return this.props.groups.map((v,k) => {
      return(
        <div className="form-check" key={"group-"+k}>
          <input className="form-check-input" type="checkbox" name="groups" value={v._id.$oid} id={"group-"+k}  checked={this.isChecked(v._id.$oid, 'groups')} onChange={this.handleToggle} />
          <label className="form-check-label" htmlFor={"group-"+k}>
            {v.name}
          </label>
        </div>
      )
    })
  }
  isChecked(val, name){
    return this.props.preset[name] ?  this.props.preset[name].some(item => item.$oid===val) : false
  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('title', this.props.preset.title);
    formData.append('description', this.props.preset.description);
    formData.append('public', this.props.preset.public);
    formData.append('image_width', this.props.preset.image_width);
    formData.append('image_height', this.props.preset.image_height);
    formData.append('image_number_of_channels', this.props.preset.image_number_of_channels);
    formData.append('num_classes', this.props.preset.num_classes);
    formData.append('task_type', this.props.preset.task_type);
    formData.append('accuracy', this.props.preset.accuracy);
    formData.append('batch_size', this.props.preset.batch_size);
    formData.append('loss', this.props.preset.loss);
    formData.append('data_set_name', this.props.preset.data_set_name);
    formData.append('data_folder', this.props.preset.data_folder);
    formData.append('data_file', this.props.preset.data_file);
    formData.append('algo_folder', this.props.preset.algo_folder);
    this.editPreset(formData)
  }
/*  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({group:
      { ...this.props.group,
        [name]: value
      }
    });
  }*/
  async editPresetRef(presetId, act, name, id){
    RestService.editPresetRef(presetId, {act: act, name: name, id: id}).then( response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.props.updatePreset(response.data.preset)

    })
  }
  async editPreset(formData){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.editPreset(this.props.preset._id.$oid, formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      // this.setState({groups: response.data.groups})
    })
  }
  handleToggle(event){
    const target = event.target;
      this.editPresetRef(this.props.preset._id.$oid, target.checked ? 'push': 'pull', target.name, target.value)
  }
  render(){
    return(
      <div>
      <h4 className="h4">Edit {this.props.preset ? this.props.preset.name : ""}</h4>
          <div>{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>

          <div className="mb-4 row">

            <div className="col-6">
              <form onSubmit={ this.handleSubmit }>
                <div className="form-label-group">

                    <input name="title" component="input" type="text" placeholder="title" className="form-control"  value={this.props.preset.title} onChange={this.props.handleChange} />
                <label htmlFor="name">Title</label>
                </div>

                <div className="form-check">
                  <input className="form-check-input" type="checkbox" name="public" id={"public"} defaultChecked={this.props.preset.public} onChange={this.props.handleChange} />
                  <label className="form-check-label" htmlFor={"public"}>
                    Publicly visible
                  </label>
                </div>
                <div className="form-label-group">
                  <textarea className="form-control"component="textarea" name="description" placeholder="Description" rows="10" value={this.props.preset.description} onChange={this.props.handleChange} ></textarea>
                </div>

                <div className="form-group">
                <label htmlFor="algo_folder">Algorithms folder: </label>
                <input  title="Leave blank to use the automatic generation. Remember to set the gridsearch settings" className="form-control form-control-sm"  type="text" id="algo_folder"
                      placeholder="Algorithms Folder" name="algo_folder" onChange={this.props.handleChange} value={this.props.preset.algo_folder} />
                </div>

                <div className="form-group">
                        <label htmlFor="data_set_name">Dataset Name</label>
                        <select title="Select a dataset. If you select Custom you have to set a data folder or a data file"
                        id="data_set_name" name="data_set_name" onChange={this.props.handleChange} className="form-control form-control-sm" value={this.props.preset.data_set_name}>
                            <option value="MNIST">MNIST</option>
                            <option value="CIFAR10">CIFAR10</option>
                            <option value="CIFAR100">CIFAR100</option>
                            <option value="CUSTOM">CUSTOM</option>
                            <option value="test_reply">test_reply</option>
                            <option value="test_maxq">test_maxq</option>
                            <option value="test_pke">test_pke</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="data_folder">Dataset folder: </label>
                        <input  title="This field is required only if you select a custom dataset." className="form-control form-control-sm"  type="text" id="data_folder"
                               placeholder="Data Folder" name="data_folder" onChange={this.props.handleChange} value={this.props.preset.data_folder}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="data_file">Dataset file: </label>
                        <input title="This field is required only if you select a custom dataset. If both file and folder are set, the folder is ignored."
                        type="text" id="data_file"
                               placeholder="Data File" name="data_file" onChange={this.props.handleChange} className="form-control form-control-sm" value={this.props.preset.data_file}/>
                        <small className="form-text text-muted">The paths should start from the shared data folder</small>
                    </div>

                  <div className="form-group">
                    <label htmlFor="task_type">Task type </label>
                    <select id="task_type" onChange={this.props.handleChange} name="task_type" className="form-control form-control-sm">
                        <option value="classification">classification</option>
                        <option value="segmentation">segmentation</option>
                        <option value="detection">detection</option>
                    </select>
                  </div>

                  <div className="form-group">
                    <label htmlFor="batch_size">Batch size</label>
                    <input type="number" id="batch_size" name="batch_size" className="form-control form-control-sm" onChange={this.props.handleChange} value={this.props.preset.batch_size}/>
                  </div>

                  <div className="form-group">
                      <label htmlFor="loss">Loss function</label>
                      <select id="loss" onChange={this.props.handleChange} name="loss" className="form-control form-control-sm" value={this.props.preset.loss}>
                          <option value="softmax">softmax</option>
                          <option value="sigmoid">sigmoid</option>
                          <option value="margin">margin</option>
                          <option value="detection_loss">detection_loss</option>
                          <option value="dice">dice</option>
                      </select>
                  </div>

                  <div className="form-group">
                    <label htmlFor="accuracy">Accuracy</label>
                    <select id="accuracy" name="accuracy" onChange={this.props.handleChange} className="form-control form-control-sm" value={this.props.preset.accuracy}>
                        <option value="IoU">IoU</option>
                        <option value="dice">dice</option>
                        <option value="mse">mse</option>
                        <option value="percent">percent</option>
                    </select>
                  </div>

                  <div className="form-group">
                      <label htmlFor="num_classes">Number of classes </label>
                      <input title="Set the number of classes required by your task" type="number" className="form-control form-control-sm" id="num_classes" name="num_classes" onChange={this.props.handleChange} value={this.props.preset.num_classes}/>
                  </div>


                  <div className="form-row">
                    <div className="col form-group">
                      <label htmlFor="image_width">Width </label>
                      <input type="number" id="image_width" name="image_width" onChange={this.props.handleChange}  className="form-control form-control-sm"value={this.props.preset.image_width}/>
                    </div>
                    <div className="col form-group">
                      <label htmlFor="image_height">Height </label>
                      <input type="number" id="image_height" name="image_height" onChange={this.props.handleChange} className="form-control form-control-sm" value={this.props.preset.image_height}/>
                    </div>
                    <div className="col form-group">
                      <label htmlFor="image_number_of_channels">Channels</label>
                      <input type="number" id="image_number_of_channels" name="image_number_of_channels" onChange={this.props.handleChange} className="form-control form-control-sm" value={this.props.preset.image_number_of_channels}/>
                    </div>
                  </div>

                <button type="submit" className="btn btn-primary">Save changes</button>
                <button type="cancel" className="btn btn-default" onClick={this.props.cancelButton}>Cancel</button>
              </form>
            </div>

            <div className="col-6">
              <h4>Assign preset to groups</h4>
              <div className="form-group">
                {this.props.groups.length > 0 ? this.placeGroups(): "There are no groups to show."}
              </div>

            </div>

          </div>

        </div>
    )
  }
}

export default EditPreset
