import React from 'react'

export default function Main(){
    return (
      <div className="col-md-9 ml-sm-auto col-lg-10 px-4">

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h2 className="h2">Admin dashboard</h2>
        </div>

        <p>Welcome to the admin dashboard. In this section you can manage different settings from the ALOHA toolflow.</p>
        <ul>
        <li><strong>Groups:</strong> You can create and delete groups, assign users and projects to them.</li>
        <li><strong>Users:</strong> If you are admin, this section you can assign administration permissions and deactivate users.</li>
        <li><strong>Presets:</strong> You can define presets for your projects.</li>
        </ul>
      </div>
    );

}
