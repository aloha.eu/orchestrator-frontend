import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"
import EditGroup from './editgroup'
import update from 'immutability-helper';

class Groups extends Component{
  constructor(props) {
      super(props);
      this.state = {name: '', description: '', message: '', status: '', groups: [], projects: [], users: [],showEdit: false, showCreate: false, currentGroup: null};
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
    this.getGroups();
    this.getProjects();
    this.getUsers();
  }
  async getProjects(){
    RestService.getProjectsByUser(this.props.user.user_id).then(response => {
      console.log(response.data)
      this.setState({projects: response.data})
      //console.log(response.data)
    })
  }
  async getUsers(){
    RestService.getUsers().then(response => {
      this.setState({users: response.data})
      //console.log(response.data)
    })
  }
  async getGroups(){
      this.setState({message: 'Loading...', status: 'loading'})
    RestService.getGroups().then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({groups: response.data.groups})
      //console.log(response.data)
    })
  }
  async createGroup(formData){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.createGroup(formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({groups: response.data.groups})
      this.setState({showCreate: false })
    })
  }
  async deleteGroup(groupId){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.deleteGroup(groupId).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({groups: this.state.groups.filter(item => item._id.$oid !== groupId)})
    })
  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('name', this.state.name);
    formData.append('description', this.state.description);
    this.createGroup(formData)
  }
  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  handleChangeGroup(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    let newstate = update(this.state, {
      groups: { [this.state.currentGroup]: { [name]: { $set: value } } }
    })
    this.setState(newstate)
  }
  updateGroup(group){
    let newstate = update(this.state, {
      groups: { [this.state.currentGroup]: { $set: group } }
    })
    this.setState(newstate)
  }
  cancelButton(event){
    this.setState({showEdit: false })
    this.setState({currentGroup: null })
  }
  table() {
    let row = this.state.groups.map( (r,k) =>{
      return(
        <tr key={k} id={`group-${r._id.$oid}`}>
          <td >{r._id.$oid}</td>
          <td>{r.name}</td>
          <td>{r.description}</td>
          <td>{r.ownership[0].username}</td>
          <td><button className="btn btn-danger" onClick={() => {this.deleteGroup(r._id.$oid)}}>Delete</button></td>
          <td><button className={"btn btn-info"+(this.state.currentGroup ? (this.state.showEdit && r._id.$oid === this.state.groups[this.state.currentGroup]._id.$oid ? "active" : "") : "")} onClick={() => {this.toggleHandler({ opened: this.state.showEdit === true ? false : true, pointer: 'showEdit', currentGroup: k})}}>Edit</button></td>
        </tr>
      )
    })
      return(
        <table className="table responsive results-list">
        <thead className="text-left">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Owner</th>
            <th>Delete</th>
            <th>Edit</th>
          </tr>
          </thead>
        <tbody>

          {row}
        </tbody>
        </table>
      )
  }

  toggleHandler(payload){
    const contpointer = payload.pointer === 'showEdit' ? 'showCreate' : 'showEdit'
    this.setState({[contpointer]: false})
    this.setState({[payload.pointer]: payload.opened})
    this.setState({currentGroup: payload.currentGroup})
  }
  render(){
    return (
      <div className="col-md-9 ml-sm-auto col-lg-10 px-4">

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h2 className="h2">Groups</h2>
              <div className="btn-toolbar mb-2 mb-md-0">
              <button type="button" className={"btn btn-sm btn-outline-secondary "+(this.state.showCreate ? "active" : "")} onClick={() => {this.toggleHandler({ opened: this.state.showCreate === true ? false : true, pointer: 'showCreate' })}}>Create group</button>
              </div>
        </div>
            <div>{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>
            <div>
              {this.state.groups && this.state.groups.length > 0 ? this.table() : 'No results to show'}
            </div>
            <div>{this.state.showEdit ? <EditGroup group={this.state.groups[this.state.currentGroup]} handleChange={this.handleChangeGroup.bind(this)} projects={this.state.projects} users={this.state.users} updateGroup={this.updateGroup.bind(this)} cancelButton={this.cancelButton.bind(this)} /> : null}
            </div>
            <div>{this.state.showCreate ? createGroup(this) : null}
            </div>

      </div>
    );
  }
}

export function createGroup(methods){
return(
  <form onSubmit={ methods.handleSubmit }>
  <h3 className="h3">Create new group</h3>

      <div className="mb-4">
      <div className="form-label-group">

          <input name="name"
                component="input"
                type="text"
                placeholder="name"
                className="form-control"
          />
      <label htmlFor="name">Group Name</label>
      </div>
      <div className="form-label-group">
      <textarea className="form-control"component="textarea" name="description" placeholder="Description" rows="10"></textarea>

        </div>
        <button type="submit" className="btn btn-primary">Create</button>
      </div>

      </form>
)

}

export default Groups
