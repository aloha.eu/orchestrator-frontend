import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"
import EditPreset from './editpreset'
import update from 'immutability-helper';

class Presets extends Component{
  constructor(props) {
      super(props);
      this.state = {createPreset: {
        title: '',
        description: '',
        public: true,
        image_width:32,
        image_height:32,
        image_number_of_channels:3,
        num_classes: 10,
        task_type:"classification",
        accuracy:"percent",
        batch_size : 64,
        loss:"softmax",
        data_set_name:"CIFAR10",
        data_folder:"",
        data_file:"",
        algo_folder:"algorithms/gridsearch",
      }, message: '', status: '', groups: [], presets: [],showEdit: false, showCreate: false, currentPreset: null};
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
    this.getPresets();
    this.getGroups();
  }
  async getPresets(){

      this.setState({message: 'Loading...', status: 'loading'})
    RestService.getPresets().then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({presets: response.data.presets})
    })
  }
  async getGroups(){
      this.setState({message: 'Loading...', status: 'loading'})
    RestService.getGroups().then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({groups: response.data.groups})
    })
  }
  async createPreset(formData){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.createPreset(formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({presets: response.data.presets})
      this.setState({showCreate: false })
    })
  }
  async deletePreset(presetId){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.deletePreset(presetId).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({presets: this.state.presets.filter(item => item._id.$oid !== presetId)})
    })
  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('title', this.state.createPreset.title);
    formData.append('description', this.state.createPreset.description);
    formData.append('public', this.state.createPreset.public);
    formData.append('image_width', this.state.createPreset.image_width);
    formData.append('image_height', this.state.createPreset.image_height);
    formData.append('image_number_of_channels', this.state.createPreset.image_number_of_channels);
    formData.append('num_classes', this.state.createPreset.num_classes);
    formData.append('task_type', this.state.createPreset.task_type);
    formData.append('accuracy', this.state.createPreset.accuracy);
    formData.append('batch_size', this.state.createPreset.batch_size);
    formData.append('loss', this.state.createPreset.loss);
    formData.append('data_set_name', this.state.createPreset.data_set_name);
    formData.append('data_folder', this.state.createPreset.data_folder);
    formData.append('data_file', this.state.createPreset.data_file);
    formData.append('algo_folder', this.state.createPreset.algo_folder);
    this.createPreset(formData)
  }
  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
        createPreset: {
          ...this.state.createPreset,
          [name]: value
        }
    });
  }
  handleChangePreset(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    let newstate = update(this.state, {
      presets: { [this.state.currentPreset]: { [name]: { $set: value } } }
    })
    this.setState(newstate)
  }
  updatePreset(preset){
    let newstate = update(this.state, {
      presets: { [this.state.currentPreset]: { $set: preset } }
    })
    this.setState(newstate)
  }
  cancelButton(event){
    this.setState({showEdit: false })
    this.setState({currentPreset: null })
  }
  table() {
    let row = this.state.presets.map( (r,k) =>{
      return(
        <tr key={k} id={`group-${r._id.$oid}`}>
          <td >{r._id.$oid}</td>
          <td>{r.title}</td>
          <td>{r.description}</td>
          <td>{Utils.convertOnOff(r.public)}</td>
          <td><button className="btn btn-danger" onClick={() => {this.deletePreset(r._id.$oid)}}>Delete</button></td>
          <td><button className={"btn btn-info"+(this.state.currentPreset ? (this.state.showEdit && r._id.$oid === this.state.presets[this.state.currentPreset]._id.$oid ? "active" : "") : "")} onClick={() => {this.toggleHandler({ opened: this.state.showEdit === true ? false : true, pointer: 'showEdit', currentPreset: k})}}>Edit</button></td>
        </tr>
      )
    })
      return(
        <table className="table responsive results-list">
        <thead className="text-left">
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th>Public</th>
            <th>Delete</th>
            <th>Edit</th>
          </tr>
          </thead>
        <tbody>
          {row}
        </tbody>
        </table>
      )
  }
  toggleHandler(payload){
    const contpointer = payload.pointer === 'showEdit' ? 'showCreate' : 'showEdit'
    this.setState({[contpointer]: false})
    this.setState({[payload.pointer]: payload.opened})
    this.setState({currentPreset: payload.currentPreset})
  }
  render(){
    return (
      <div className="col-md-9 ml-sm-auto col-lg-10 px-4">

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h2 className="h2">Presets</h2>
              <div className="btn-toolbar mb-2 mb-md-0">
              <button type="button" className={"btn btn-sm btn-outline-secondary "+(this.state.showCreate ? "active" : "")} onClick={() => {this.toggleHandler({ opened: this.state.showCreate === true ? false : true, pointer: 'showCreate' })}}>New preset</button>
              </div>
        </div>
            <div>{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>
            <div>
              {this.state.presets && this.state.presets.length > 0 ? this.table() : 'No results to show'}
            </div>
            <div>{this.state.showEdit ? <EditPreset preset={this.state.presets[this.state.currentPreset]} handleChange={this.handleChangePreset.bind(this)} presets={this.state.presets} groups={this.state.groups} updatePreset={this.updatePreset.bind(this)} cancelButton={this.cancelButton.bind(this)} /> : null}
            </div>
            <div>{this.state.showCreate ? createPreset(this) : null}
            </div>

      </div>
    );
  }
}

export function createPreset(methods){
return(
  <form onSubmit={ methods.handleSubmit }>
  <h3 className="h3">Create new preset</h3>

      <div className="mb-4">
        <div className="form-label-group">
          <input name="title" component="input"  type="text"  placeholder="Title" className="form-control" value={methods.state.createPreset.title} onChange={methods.handleChange} />
          <label htmlFor="name">Preset Title</label>
        </div>

        <div className="form-check">
          <input className="form-check-input" type="checkbox" name="public" id={"public"} defaultChecked={methods.state.createPreset.public} onChange={methods.handleChange} />
          <label className="form-check-label" htmlFor={"public"}>
            Publicly visible
          </label>
        </div>
        <div className="form-label-group">
          <textarea className="form-control"component="textarea" name="description" placeholder="Description" rows="10" value={methods.state.createPreset.description} onChange={methods.handleChange} ></textarea>
        </div>

        <div className="form-group">
        <label htmlFor="algo_folder">Algorithms folder: </label>
        <input  title="Leave blank to use the automatic generation. Remember to set the gridsearch settings" className="form-control form-control-sm"  type="text" id="algo_folder"
              placeholder="Algorithms Folder" name="algo_folder" onChange={methods.handleChange} value={methods.state.createPreset.algo_folder} />
        </div>

        <div className="form-group">
                <label htmlFor="data_set_name">Dataset Name</label>
                <select title="Select a dataset. If you select Custom you have to set a data folder or a data file"
                id="data_set_name" name="data_set_name" onChange={methods.handleChange} className="form-control form-control-sm" value={methods.state.createPreset.data_set_name}>
                    <option value="MNIST">MNIST</option>
                    <option value="CIFAR10">CIFAR10</option>
                    <option value="CIFAR100">CIFAR100</option>
                    <option value="CUSTOM">CUSTOM</option>
                    <option value="test_reply">test_reply</option>
                    <option value="test_maxq">test_maxq</option>
                    <option value="test_pke">test_pke</option>
                </select>
            </div>
            <div className="form-group">
                <label htmlFor="data_folder">Dataset folder: </label>
                <input  title="This field is required only if you select a custom dataset." className="form-control form-control-sm"  type="text" id="data_folder"
                       placeholder="Data Folder" name="data_folder" onChange={methods.handleChange} value={methods.state.createPreset.data_folder}/>
            </div>
            <div className="form-group">
                <label htmlFor="data_file">Dataset file: </label>
                <input title="This field is required only if you select a custom dataset. If both file and folder are set, the folder is ignored."
                type="text" id="data_file"
                       placeholder="Data File" name="data_file" onChange={methods.handleChange} className="form-control form-control-sm" value={methods.state.createPreset.data_file}/>
                <small className="form-text text-muted">The paths should start from the shared data folder</small>
            </div>

          <div className="form-group">
            <label htmlFor="task_type">Task type </label>
            <select id="task_type" onChange={methods.handleChange} name="task_type" className="form-control form-control-sm">
                <option value="classification">classification</option>
                <option value="segmentation">segmentation</option>
                <option value="detection">detection</option>
            </select>
          </div>

          <div className="form-group">
            <label htmlFor="batch_size">Batch size</label>
            <input type="number" id="batch_size" name="batch_size" className="form-control form-control-sm" onChange={methods.handleChange} value={methods.state.createPreset.batch_size}/>
          </div>

          <div className="form-group">
              <label htmlFor="loss">Loss function</label>
              <select id="loss" onChange={methods.handleChange} name="loss" className="form-control form-control-sm" value={methods.state.createPreset.loss}>
                  <option value="softmax">softmax</option>
                  <option value="sigmoid">sigmoid</option>
                  <option value="margin">margin</option>
                  <option value="detection_loss">detection_loss</option>
                  <option value="dice">dice</option>
              </select>
          </div>

          <div className="form-group">
            <label htmlFor="accuracy">Accuracy</label>
            <select id="accuracy" name="accuracy" onChange={methods.handleChange} className="form-control form-control-sm" value={methods.state.createPreset.accuracy}>
                <option value="IoU">IoU</option>
                <option value="dice">dice</option>
                <option value="mse">mse</option>
                <option value="maP">maP</option>
                <option value="percent">percent</option>
            </select>
          </div>

          <div className="form-group">
              <label htmlFor="num_classes">Number of classes </label>
              <input title="Set the number of classes required by your task" type="number" className="form-control form-control-sm" id="num_classes" name="num_classes" onChange={methods.handleChange} value={methods.state.createPreset.num_classes}/>
          </div>


          <div className="form-row">
            <div className="col form-group">
              <label htmlFor="image_width">Width </label>
              <input type="number" id="image_width" name="image_width" onChange={methods.handleChange}  className="form-control form-control-sm"value={methods.state.createPreset.image_width}/>
            </div>
            <div className="col form-group">
              <label htmlFor="image_height">Height </label>
              <input type="number" id="image_height" name="image_height" onChange={methods.handleChange} className="form-control form-control-sm" value={methods.state.createPreset.image_height}/>
            </div>
            <div className="col form-group">
              <label htmlFor="image_number_of_channels">Channels</label>
              <input type="number" id="image_number_of_channels" name="image_number_of_channels" onChange={methods.handleChange} className="form-control form-control-sm" value={methods.state.createPreset.image_number_of_channels}/>
            </div>
          </div>

          <button type="submit" className="btn btn-primary">Create</button>
      </div>

    </form>
)

}

export default Presets
