import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"
import update from 'immutability-helper';
import Toggle from 'react-toggle'

class Users extends Component{
  constructor(props) {
      super(props);
      this.state = {name: '', surname: '', username: '', email: '', message: '', status: '', users: [],showEdit: false, showCreate: false, currentGroup: null};
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
    this.getUsers();
  }

  async getUsers(){
    RestService.getUsers().then(response => {
      this.setState({users: response.data})
      console.log(response.data)
    })
  }
  async createUser(formData){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.createUser(formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({users: response.data.users})
      this.setState({showCreate: false })
    })
  }
  async deleteUser(userId){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.deleteUser(userId).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({users: this.state.users.filter(item => item.id !== userId)})
    })
  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('name', this.state.name);
    formData.append('surname', this.state.surname);
    formData.append('email', this.state.email);
    formData.append('username', this.state.username);
    this.createUser(formData)
  }
  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  handleChangeGroup(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    let newstate = update(this.state, {
      groups: { [this.state.currentGroup]: { [name]: { $set: value } } }
    })
    this.setState(newstate)
  }
  updateGroup(group){
    let newstate = update(this.state, {
      groups: { [this.state.currentGroup]: { $set: group } }
    })
    this.setState(newstate)
  }
  cancelButton(event){
    this.setState({showEdit: false })
    this.setState({currentGroup: null })
  }
  handleToggleChange(event) {
    //console.log(event.target.dataset.proj)
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const user = event.target.dataset.user
    const kuser = event.target.dataset.key

    RestService.updateUserAdmin({usertId: user, property: name, value: value}).then(response => {
      let newstate = update(this.state, {
        users: { [kuser]: { [name]: { $set: value } } }
      })
      this.setState(newstate)
    })

  }
  table() {
    let row = this.state.users.map( (r,k) =>{
      return(
        <tr key={k} id={`user-${r.id}`}>
          <td >{r.username}</td>
          <td>{r.name+ ' '+r.surname}</td>
          <td>{r.email}</td>
          <td><Toggle
            data-user={r.id}
            data-key={k}
            checked={r.admin}
            name='admin'
            onChange={this.handleToggleChange.bind(this)}
            icons={false} /></td>
          <td><button className="btn btn-danger" onClick={() => {this.deleteUser(r.id)}}>Delete</button></td>
        </tr>
      )
    })
      return(
        <table className="table responsive results-list">
        <thead className="text-left">
          <tr>

            <th>Username</th>
            <th>Name</th>
            <th>Email</th>
            <th>Admin</th>
            <th>Delete</th>
          </tr>
          </thead>
        <tbody>

          {row}
        </tbody>
        </table>
      )
  }
  toggleHandler(payload){
    this.setState({[payload.pointer]: payload.opened})
    this.setState({currentGroup: payload.currentGroup})
  }
  render(){
    return (
      <div className="col-md-9 ml-sm-auto col-lg-10 px-4">

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h2 className="h2">Users</h2>
              <div className="btn-toolbar mb-2 mb-md-0">
              <button type="button" className={"btn btn-sm btn-outline-secondary "+(this.state.showCreate ? "active" : "")} onClick={() => {this.toggleHandler({ opened: this.state.showCreate === true ? false : true, pointer: 'showCreate' })}}>Create user</button>
              </div>
        </div>
            <div>{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>
            <div>
              {this.state.users && this.state.users.length > 0 ? this.table() : 'No results to show'}
            </div>
            <div>{this.state.showCreate ? createUser(this) : null}
            </div>

      </div>
    );
  }
}

export function createUser(methods){
return(
  <form onSubmit={ methods.handleSubmit }>
  <h3 className="h3">Create new user</h3>

      <div className="mb-4">
      <div className="form-label-group">
          <input name="name"
                component="input"
                type="text"
                placeholder="name"
                className="form-control"
          />
          <label htmlFor="name">Name</label>
      </div>
      <div className="form-label-group">
          <input name="surname"
                component="input"
                type="text"
                placeholder="Surname"
                className="form-control"
          />
          <label htmlFor="surname">Surname</label>
      </div>
      <div className="form-label-group">
          <input name="username"
                component="input"
                type="text"
                placeholder="username"
                className="form-control"
          />
          <label htmlFor="username">Username</label>
      </div>
      <div className="form-label-group">
          <input name="email"
                component="input"
                type="text"
                placeholder="email"
                className="form-control"
          />
          <label htmlFor="email">Email</label>
      </div>

        <button type="submit" className="btn btn-primary">Create</button>
      </div>

      </form>
)

}

export default Users
