import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"

class EditGroup extends Component{
  constructor(props) {
      super(props);
      this.state = { message: '',
                    status: '',
                    projects: this.props.projects,
                    users:this.props.users}
      this.handleSubmit = this.handleSubmit.bind(this);
      //this.handleChange = this.handleChange.bind(this);
      this.handleToggle = this.handleToggle.bind(this);
  }
  placeUsers(){
    return this.state.users.map((v,k) => {
      return(
        <div className="form-check" key={"user-"+k}>
          <input className="form-check-input" type="checkbox" name="users" value={v.id} id={"user-"+k}  checked={this.isChecked(v.id, 'users')} onChange={this.handleToggle} />
          <label className="form-check-label" htmlFor={"user-"+k}>
            {v.username+" ("+v.name+" "+v.surname+")"}
          </label>
        </div>
      )
    })
  }
  isChecked(val, name){
    return this.props.group[name] ?  this.props.group[name].some(item => item.$oid===val) : false
  }
  placeProjects(){
    return this.state.projects.map((v,k) => {
      return(
        <div className="form-check" key={"project-"+k}>
          <input className="form-check-input" type="checkbox" name="projects" value={v.id} id={"project-"+k} checked={this.isChecked(v.id, 'projects')} onChange={this.handleToggle} />
          <label className="form-check-label" htmlFor={"project-"+k}>
            {v.title}
          </label>
        </div>
      )
    })
  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('name', this.props.group.name);
    formData.append('description', this.props.group.description);
    this.editGroup(formData)
  }
/*  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({group:
      { ...this.props.group,
        [name]: value
      }
    });
  }*/
  async editGroupRef(groupId, act, name, id){
    RestService.editGroupRef(groupId, {act: act, name: name, id: id}).then( response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.props.updateGroup(response.data.group)
    })
  }
  async editGroup(formData){
    this.setState({message: 'Loading...', status: 'loading'})
    RestService.editGroup(this.props.group._id.$oid, formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      // this.setState({groups: response.data.groups})
    })
  }
  handleToggle(event){
    const target = event.target;
      this.editGroupRef(this.props.group._id.$oid, target.checked ? 'push': 'pull', target.name, target.value)
  }
  render(){
    return(
      <div>
      <h4 className="h4">Edit {this.props.group.name}</h4>
          <div>{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>

          <div className="mb-4 row">

            <div className="col-6">
              <form onSubmit={ this.handleSubmit }>
                <div className="form-label-group">

                    <input name="name"
                          component="input"
                          type="text"
                          placeholder="name"
                          className="form-control"
                          value={this.props.group.name}
                          onChange={this.props.handleChange}
                    />
                <label htmlFor="name">Group Name</label>
                </div>
                <div className="form-label-group">
                <textarea className="form-control"component="textarea" name="description" placeholder="Description" rows="10" onChange={this.props.handleChange} value={this.props.group.description}></textarea>
                </div>
                <button type="submit" className="btn btn-primary">Save changes</button>
                <button type="cancel" className="btn btn-default" onClick={this.props.cancelButton}>Cancel</button>
              </form>
            </div>

            <div className="col-6">
              <h4>Assign users</h4>
              <div className="form-group">
                {this.state.users.length > 0 ? this.placeUsers(): "There are no users to show."}
              </div>

              <h4>Assign projects</h4>
              <div className="form-group">
                {this.state.projects.length > 0 ? this.placeProjects() : "There are no projects to show."}
              </div>
            </div>

          </div>

        </div>
    )
  }
}

export default EditGroup
