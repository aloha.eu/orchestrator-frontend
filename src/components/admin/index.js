import React from 'react'
import { Route, useRouteMatch, useParams, Switch, NavLink } from 'react-router-dom';
import { connect } from 'react-redux'
import '../../assets/admin.css'
import Main from './main'
import Groups from './groups'
import Presets from './presets'
import Users from './users'

const AdminMain = ({ user }) => {
    let { path, url } = useRouteMatch();
    return (
      <div>
        <nav className="col-md-2 bg-light sidebar">

        <div className="sidebar-sticky">
          <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Administration</span>
          </h6>
          <ul className="nav flex-column">
            <li className="nav-item"><NavLink exact to={`${url}`} className={"nav-link"} activeClassName="active">Main</NavLink></li>
            <li className="nav-item"><NavLink exact to={`${url}/groups`} className="nav-link" activeClassName="active">Groups</NavLink></li>
            {user.admin ? <li className="nav-item"><NavLink exact to={`${url}/users`} className="nav-link" activeClassName="active">Users</NavLink></li> : null}
          </ul>
            <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Project Specifics</span>
            </h6>
          <ul className="nav flex-column">
            <li className="nav-item"><NavLink exact to={`${url}/presets`} className="nav-link" activeClassName="active">Presets</NavLink></li>
          </ul>
        </div>
        </nav>
              <Switch>
              <Route exact path={path} url={url} component={Main} />
              <Route path={`${path}/:topicId`}>
                <AdminRoutes user={user} />
              </Route>
              </Switch>
      </div>
    );
}

function AdminRoutes({user}) {
  // The <Route> that rendered this component has a
  // path of `/topics/:topicId`. The `:topicId` portion
  // of the URL indicates a placeholder that we can
  // get from `useParams()`.
  let { topicId } = useParams();
  switch (topicId) {
    case 'groups':
      return (<Groups user={user} />)
    case 'presets':
      return (<Presets />)
    case 'users':
      return (<Users />)
    default:
      return (<Main />)
  }
}

// sagas communications
const mapStateToProps = state => {
  return {user: state.auth.user};
};


export default connect( mapStateToProps)(AdminMain);
