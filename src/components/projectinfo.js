import React, {Component} from 'react'
import PropTypes from 'prop-types'; // ES6
import {RestService} from "../service/restService";
import '../assets/index.css'
import Utils from '../utils'
import { ALOHA_FRONTEND_CONFIGURATION } from '../configuration/config.js';
import Iframe from 'react-iframe'
import {
  ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, LineChart, Line, Legend
} from 'recharts';

class TrainResults extends Component {

    constructor(props) {
        super(props);

        this.state = {
          popUpOpened: false,
          project_result:[],
          results:[],
            num_alg: 0,
          sesame_result: "",
          netron_server: false,
        sel_result: {
          sesame_perf_cost: "",
          sesame_pareto: ""
        }};

        this.num_alg_tot =0;

        this.handleChange = this.handleChange.bind(this)
    }
    componentDidMount(){
      this.loadResults()
    }
    async loadResults() {
        let project = await RestService.getProjectResult(this.props.project.id);

        this.setState({results: project.data})
        this.selectResult()
    }
    selectResult(){
      const results = this.state.results.map(r => {
        //console.log(r)
        //r = JSON.parse(r)
        if (r.id === this.props.project.model) {
          return r
        }else{return false}
      })
      const sel_result = results.find(result => result !== false)
      this.setState({sel_result: sel_result})
      //console.log(sel_result)
       if(sel_result) {
         this.setState({sesame_result: sel_result.maps[0]})
         if(sel_result.onnx !== undefined){this.callNetron(sel_result.onnx)}
       }
      }
    lineChart(){
      let values = []
      if(this.state.sel_result.sesame_perf_cost !== ""){
        if(this.state.sel_result.sesame_perf_cost !== "None"){
          values = Utils.toObject(this.state.sel_result.sesame_perf_cost)
          values.data = []
          values[0].points.forEach((p,k) =>{
            const val = {name: p[0], [values[0].y_label]: p[1]}
            values.data.push(val)
          })
          values[1].points.forEach((p,k) =>{
            values.data[k][values[1].y_label] = p[1]
          })
          return(
            <ResponsiveContainer minWidth={150} height={200}>
            <LineChart width={400} height={200} data={values.data} >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey={values[0].y_label} stroke="#8884d8" />
                <Line type="monotone" dataKey={values[1].y_label} stroke="#82ca9d" />
                </LineChart>
            </ResponsiveContainer>
          )
        }else{
          return <div className="alert alert-secondary">Not cost information yet.</div>
        }
      }else{
        return <div className="alert alert-secondary">Waiting for results.</div>
      }

    }
    scatterChart(){
      let values = []
      if( this.state.sel_result.sesame_pareto !== "None" && this.state.sel_result.sesame_pareto !== ""){
        values =  Utils.toObject(this.state.sel_result.sesame_pareto)
        if(values.name !== undefined){
          values.data = []
          values.points.forEach((p,k) =>{
            const val = {[values.x_label]: p[0], [values.y_label]: p[1]}
            values.data.push(val)
          })
          //console.log(values.data)
          return(
            <ResponsiveContainer minWidth={150} height={200}>
            <ScatterChart  width={400}height={200} >
              <CartesianGrid />
              <XAxis type="number" dataKey={values.x_label} name={values.x_label} />
              <YAxis type="number" dataKey={values.y_label} name={values.y_label} />
              <Tooltip />
              <Scatter name="A school" data={values.data} fill="#8884d8" />
            </ScatterChart>
          </ResponsiveContainer>
          )
        }else{
          return <div className="alert alert-secondary">Not pareto information yet.</div>
        }
      }else{
        return <div className="alert alert-secondary">Waiting for results.</div>
      }
    }
    async callNetron(filepath){
      const netron = await RestService.openNetron(filepath)
      //console.log(netron)
      if(netron.status === 200){
        this.setState({netron_server: true})
      }
    }
    sesameResults(){
      //console.log(this.state.sel_result.maps)
      let maps;
      if(this.state.sel_result){
      if(this.state.sel_result.maps !== undefined){
        maps = this.state.sel_result.maps.map((p,k) =>{
          return(
            <option key={k} value={p}>Map {k}</option>
          )
        })
      }else{
       maps = "<option>No value yet</option>"
     }}else{
       maps = <option>No value yet</option>
     }
      return(
        <select title="Accuracy selector"
        id="sesame_result" name="sesame_result" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.sesame_result}>
          {maps}
        </select>
      )
    }

    projectDetails(data){
     Object.keys(data).map(key =>
        <div className="pb-2 pt-1 border-top" key={key}>{key+": "+data[key]}</div>
      )
    const results = this.state.sel_result ? Object.keys(this.state.sel_result).map(key =>
       <div className="pb-2 pt-1 border-top" key={key}>{key+": "+data[key]}</div>
     ) : ''
      return(
        <div id="project-details">
        <p>Architecture: {data.architecture}</p>
        <p>Dataset: {data.dataset}</p>
        <p>Model: {data.model}</p>
        <p>
          <span className={data.per_en === true ? "badge badge-pill badge-success" : "badge badge-pill badge-light"}>Performance</span>
          <span className={data.tra_en === true ? "badge badge-pill badge-success" : "badge badge-pill badge-light"}>Training</span>
          <span className={data.sec_en === true ? "badge badge-pill badge-success" : "badge badge-pill badge-light"}>Security</span>
          <span className={data.rpi_en === true ? "badge badge-pill badge-success" : "badge badge-pill badge-light"}>Parsimonious</span>
          <span className={data.sesame_en === true ? "badge badge-pill badge-success" : "badge badge-pill badge-light"}>SESAME</span>
          <span className={data.PI_en === true ? "badge badge-pill badge-success" : "badge badge-pill badge-light"}>PI</span>
        </p>
          <h6>Project Results</h6>
          {results}

        </div>
      )
    }
    handleChange(event){
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      //console.log(value)
      //console.log(name)
      this.setState({
        [name]: value
      });
  }

    downloadFile(filePath){
      //  this.props.alert.success("Downloading file")
        let response = RestService.getProjectFile(filePath);
        console.log(response)
    }

    downloadImage(imagePath){
    //    this.props.alert.success("Downloading results image")
        let response = RestService.getProjectFile(imagePath);
        console.log(response)
    }

    closeModal() {
      this.setState({ // set popup status
          popUpOpened: false
      })
    }

    render() {

       // var sf = "file:///home/nur/docker_arch/shared_data/"
        //var training_img_path = sf + this.state.project_result.training_img_path
        //var tensorboard_url = ALOHA_FRONTEND_CONFIGURATION['tensorboardUrl'] + "#scalars&regexInput="+this.props.project + "&_smoothingWeight=0"
        return (
            <div className="results container-fluid">

                  <div className="row"><p>Algorithm Configuration summary</p></div>
                <div className="row mb-3"><h5>{this.props.project.title} <small>(ID: {this.props.project.id})</small></h5></div>

              <div className="row">
                <div className="col-sm-4">
                  <h6>Project Details</h6>
                  {this.projectDetails(this.props.project)}
                </div>
                <div className="col-sm-4">
                <h6>SESAME Performance cost</h6>
                {this.lineChart()}
                  <h6>Used ONNX</h6>
                  {this.state.netron_server === true ?
                  <Iframe url={ALOHA_FRONTEND_CONFIGURATION['netronUrl'] + "?onnx_path="+this.state.sel_result.onnx}
                    width="100%"
                    height="400"
                    id="myIframe"
                    display="initial"
                    className="border-0"
                    position="relative"/>
                    : <div className="alert alert-secondary">Netron server is offline</div> }
                </div>
                <div className="col-sm-4">
                  <h6>SESAME Pareto</h6>
                  {this.scatterChart()}
                  <h6>SESAME Results</h6>
                  {this.sesameResults()}
                  {this.state.sesame_result ?
                  <img src={`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/file?filePath=${this.state.sesame_result}`} className="img-fluid rounded" alt="Sesame Result" />
                  :
                  <div className="alert alert-secondary">No image to show.</div>}
                </div>
              </div>
            </div>


    )
  }
}

TrainResults.propTypes={
    updateProject: PropTypes.func,
}
export default TrainResults;
