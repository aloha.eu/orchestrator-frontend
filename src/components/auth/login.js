import React, {Component} from 'react';
import { Link} from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { signInAction } from '../../redux/actions/auth';
import { connect } from 'react-redux';
import '../../assets/floating-labels.css';


const validate = values => {
    const errors = {}

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    if (!values.password) {
        errors.password = 'Required'
      } else if (values.password.length < 2) {
        errors.passowrd = 'Minimum be 2 characters or more'
      }
    return errors
  }
const renderField = ({ input, label, type, tolabel, meta: { touched, error, warning } }) => (
    <div className="form-label-group">
        <input {...input} placeholder={label} type={type} className={touched ? (error ? "form-control is-invalid" : "form-control is-valid"): "form-control"} />
        {touched && ((error && <span className="text-danger">{error}</span>) || (warning && <span>{warning}</span>))}
        <label htmlFor={tolabel}>{label}</label>
    </div>
  )
class Login extends Component {
  submit = (values) => {
    this.props.signInAction(values, this.props.history);
    }

    errorMessage() {
      if (this.props.errorMessage) {
        return (
          <div className="alert alert-warning">
            {this.props.errorMessage}
          </div>
        );
      }
    }

  render(){
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={ handleSubmit(this.submit) } className="form-signin">
      <h2 className="h3 mb-3 font-weight-normal">Sign In</h2>
      <div>
        {this.errorMessage()}
      </div>
          <div className="text-center mb-4">
              <Field tolabel="email" name="email" type="text" component={renderField} label="Email" />
              <Field tolabel="password" name="password" type="password" component={renderField} label="Password" />
            <button type="submit" className="btn btn-primary">Sign In</button>
          </div>

            <Link to="/recover" key="recover">Recover Password</Link>
          </form>
    );
  }
}
function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}
const reduxFormSignin = reduxForm({
  form: 'login',
  validate
})(Login);

export default connect(mapStateToProps, {signInAction})(reduxFormSignin);
