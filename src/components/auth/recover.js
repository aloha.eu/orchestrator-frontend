import React, {Component} from 'react'
import { Field, reduxForm } from 'redux-form';
import { recoverAction, recoverPass } from '../../redux/actions/auth';
import { connect } from 'react-redux';
import '../../assets/floating-labels.css'
import {RestService} from "../../service/restService";


const validate = values => {
    const errors = {}

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }

    if (!values.password) {
        errors.password = 'Required'
      } else if (values.password.length < 2) {
        errors.passowrd = 'Minimum be 2 characters or more'
      }
    if (values.password !== values.password_ver) {
      errors.password_ver = 'Both passwords must be equal'
    }
    return errors
  }
const renderField = ({ input, label, type, tolabel, helper, meta: { touched, error, warning } }) => (
    <div className="form-label-group">
        <input {...input} placeholder={label} type={type} className={touched ? (error ? "form-control is-invalid" : "form-control is-valid"): "form-control"} />
        {touched && ((error && <span className="text-danger">{error}</span>) || (warning && <span>{warning}</span>))}
        <label htmlFor={tolabel}>{label}</label>
        <small className="form-text text-muted text-left">{helper}</small>
    </div>
  )

class Recover extends Component {
  constructor(props) {
      super(props);
      this.state = {hash: this.props.match.params.hash, valid: false, user: {}};
  }


  submit = (values) => {
    values.hash = this.props.match.params.hash
    values.email ? this.props.recoverAction(values, this.props.history) : this.props.recoverPass(values, this.props.history);
    }

    errorMessage() {
      if (this.props.errorMessage) {
        return (
          <div className="alert alert-warning">
            {this.props.errorMessage}
          </div>
        );
      }
    }
  async verifyHash(){
    RestService.verifyHash({hash: this.state.hash}).then( response =>{
        if(response.data.status === 'success'){
           this.setState({valid: true})
           this.setState({user: response.data.data})
         }else{ console.log(response.data.message) }
    })
  }
  generateHash(){
    if(this.state.hash && this.state.valid === true){
      return(
        <div className="text-center mb-4">
        <p>{this.state.user.username}, insert your new password</p>

        <Field tolabel="password" name="password" type="password" component={renderField} label="Password" helper="  Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji."/>
        <Field tolabel="password_ver" name="password_ver" type="password" component={renderField} label="Repeat password" />

        <button type="submit" className="btn btn-primary">Generate new password</button>
        </div>
      )
    }else if(this.state.hash && this.state.valid === false ){
      this.verifyHash()
      return(
        <div className="text-center mb-4">
        <p>Validating your recover token...</p>
        </div>
      )
    }else{
      return(
          <div className="text-center mb-4">
          <p>Insert your email and you will receieve an email to change your password.</p>

          <Field tolabel="email" name="email" type="text" component={renderField} label="Email" />

          <button type="submit" className="btn btn-primary">Recover</button>
          </div>
      )
    }
  }
  render(){
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={ handleSubmit(this.submit) } className="form-signin">
      <h2 className="h3 mb-3 font-weight-normal">Recover your password</h2>
      {this.generateHash()}

            <div>
              {this.errorMessage()}
            </div>
          </form>
    );
  }
}
function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}
const reduxFormSignin = reduxForm({
  form: 'recover',
  validate
})(Recover);

export default connect(mapStateToProps, {recoverAction, recoverPass})(reduxFormSignin);
