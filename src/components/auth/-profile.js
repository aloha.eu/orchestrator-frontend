import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"
import {connect} from 'react-redux'
import Moment from 'react-moment';
import { signOutAction } from '../../redux/actions/auth';


class Profile extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {
        name: '',
        surname: '',
        email: '',
        username: '',
        modified_date: '',
        creation_date: '',
        active: true,
        admin: false
      },
      password: '',
      password_ver: '',
      message: '',
      status: ''
    }
    this.handleSubmitUser = this.handleSubmitUser.bind(this);
    this.handleChangeUser = this.handleChangeUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){

  this.getUser()
  }
  async getUser(){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.getUser(this.props.user_id).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({user: response.data.user})
    })

  }
  async updateUser(formData){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.updateUser(this.props.user_id, formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({user: response.data.user})
    })
  }
  async changePassword(formData){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.changePassword(formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      if(response.data.status === 'success'){
        alert(response.data.message);
        setTimeout(() =>{ this.props.signOutAction()}, 3000);
      }
    })

  }
  handleSubmitUser(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('name', this.state.user.name);
    formData.append('surname', this.state.user.surname);
    formData.append('username', this.state.user.username);
    formData.append('email', this.state.user.email);
    this.updateUser(formData)
  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('password', this.password);
    formData.append('password_ver', this.state.password_ver);
    formData.append('email', this.state.user.email);
    this.changePassword(formData)
  }
  handleChangeUser(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({ user:
      { ...this.state.user,
        [name]: value
      }
    });
    //console.log(`Changing: ${value} in ${name}`)
  }
  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
        [name]: value
    });
  }
  showDates(){
    return(
      <div className="text-muted mt-3">
        <p>Created: <Moment unix format="DD-mm-YYYY mm:ss">{this.state.user.creation_date ? this.state.user.creation_date.$date : null}</Moment></p>
        <p>Modified: <Moment unix format="DD-mm-YYYY mm:ss">{this.state.user.modified_date ? this.state.user.modified_date.$date : null}</Moment></p>
      </div>
    )
  }
  render() {
    return (
      <div className="container">
            <h2 className="h2">Profile</h2>
            <div className="row">{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>
            <div className="row">
              <p>  Hi {this.props.username}. This is your profile. You can make changes.</p>
            </div>
            <form onSubmit={ this.handleSubmitUser }>
                <div className="mb-4">
                <div className="form-label-group">
                    <input name="name"
                          type="text"
                          className="form-control"
                          value={this.state.user.name}
                          onChange={this.handleChangeUser}  />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-label-group">
                <input name="surname"
                      type="text"
                      className="form-control"
                      value={this.state.user.surname}
                      onChange={this.handleChangeUser}  />
                <label htmlFor="surname">Surname</label>
                </div>

                <div className="form-label-group">
                <input name="email"
                      type="text"
                      className="form-control"
                      value={this.state.user.email}
                      onChange={this.handleChangeUser}  />
                <label htmlFor="email">Email</label>
                </div>

                <div className="form-label-group">
                <input name="username"
                      type="text"
                      className="form-control"
                      value={this.state.user.username}
                      onChange={this.handleChangeUser}  />
                <label htmlFor="username">Username</label>
                </div>
                  <button type="submit" className="btn btn-primary">Submit changes</button>
                </div>
                </form>

                <h3 className="h3">Change password</h3>

                <form onSubmit={ this.handleSubmit }>
                    <div className="mb-4">
                    <div className="form-label-group">
                        <input name="password"
                              type="password"
                              className="form-control"
                              value={this.state.password}
                              onChange={this.handleChange}  />
                    <label htmlFor="password">Password</label>
                    </div>
                    <div className="form-label-group">
                    <input name="password_ver"
                          type="password"
                          className="form-control"
                          value={this.state.password_ver}
                          onChange={this.handleChange}  />
                    <label htmlFor="password_ver">Repeat new password</label>
                    </div>

                    <input name="email"
                          type="hidden"
                          className="form-control"
                          value={this.state.user.email} />
                      <button type="submit" className="btn btn-primary">Submit changes</button>
                    </div>

                    </form>
                    <div>
                      <p>Active: {this.state.user.active ? 'Yes' : 'No'}</p>
                      <p>Admin: {this.state.user.admin ? 'Yes' : 'No'}</p>
                      {this.showDates()}
                    </div>
      </div>


    );
    }
}
const mapStateToProps = state => {
  return {user_id: state.auth.user.user_id,  username: state.auth.user.username};
};
export default connect(mapStateToProps, {signOutAction})(Profile)
