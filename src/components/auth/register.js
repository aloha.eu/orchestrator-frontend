import React, {Component} from 'react'
import { Field, reduxForm } from 'redux-form';
import { registerUser } from '../../redux/actions/auth';
import { connect } from 'react-redux';
import '../../assets/floating-labels.css'

const validate = values => {
    const errors = {}
    console.log(values)
    if (!values.surname) {
        errors.surname= 'Required'
      } else if (values.surname.length < 2) {
        errors.surname = 'Minimum be 2 characters or more'
      }
    if (!values.name) {
        errors.name= 'Required'
      } else if (values.name.length < 2) {
        errors.name = 'Minimum be 2 characters or more'
      }
    if (!values.username) {
        errors.username= 'Required'
      } else if (values.username.length < 2) {
        errors.username = 'Minimum be 2 characters or more'
      }

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    if (!values.password) {
        errors.password = 'Required'
      } else if (values.password.length < 2) {
        errors.passowrd = 'Minimum be 2 characters or more'
      }
    if (values.password !== values.password_ver) {
      errors.password_ver = 'Both passwords must be equal'
    }
    if(values.accept_registration !== true){
        errors.accept_registration = 'You have to agree our privacy statements.'
    }
    return errors
  }

const renderField = ({ input, label, type, tolabel, helper, meta: { touched, error, warning } }) => (
    <div className={type==="checkbox" ? "form-check" : "form-label-group"}>
        {type==="checkbox" ?  <input {...input} placeholder={label} type={type} className={touched ? (error ? "is-invalid form-check-input" : "is-valid form-check-input"): "form-check-input"} /> : <input {...input} placeholder={label} type={type} className={touched ? (error ? "form-control is-invalid" : "form-control is-valid"): "form-control"} />}
        {touched && ((error && <span className="text-danger">{error}</span>) || (warning && <span>{warning}</span>))}
        <label htmlFor={tolabel} className={type==="checkbox" ? "form-check-label small text-left": ""}>{label}</label>
        <small className="form-text text-muted text-left">{helper}</small>
    </div>
  )

class Register extends Component {
  submit = (values) => {
    this.props.registerUser(values, this.props.history);
    }

    errorMessage() {
      if (this.props.errorMessage) {
        return (
          <div className="alert alert-warning">
            {this.props.errorMessage}
          </div>
        );
      }
    }

  render(){
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={ handleSubmit(this.submit) } className="form-signin">
      <h2 className="h3 mb-3 font-weight-normal">Register</h2>
        <div>
          {this.errorMessage()}
        </div>
          <div className="text-center mb-4">
            <Field tolabel="name" name="name" type="text" component={renderField} label="First Name" />
            <Field tolabel="surname" name="surname" type="text" component={renderField} label="Last Name" />
            <Field tolabel="username" name="username" type="text" component={renderField} label="Username" />

            <Field tolabel="email" name="email" type="text" component={renderField} label="Email" />
            <Field tolabel="password" name="password" type="password" component={renderField} label="Password" helper="Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji."/>
            <Field tolabel="password_ver" name="password_ver" type="password" component={renderField} label="Repeat password" />

            <div><Field tolabel="accept_registration" name="accept_registration" type="checkbox" component={renderField} label="I agree with the ALOHA User Community Online Privacy Statement." />
            <small>You can more about read our <a href="https://www.aloha-h2020.eu/project/get-involved#privacystatement">privacy statements</a>.</small>
            </div>

            <button type="submit" className="btn btn-primary">Register</button>
          </div>

          </form>
    );
  }
}
function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}
const reduxFormRegister = reduxForm({
  form: 'Register',
  validate
})(Register);

export default connect(mapStateToProps, {registerUser})(reduxFormRegister);
