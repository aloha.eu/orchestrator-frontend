import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink, Link} from 'react-router-dom';
import { signOutAction } from '../redux/actions/auth';
import { clearData } from '../redux/actions/board';


class Navbar extends Component {
  signOut(){
    this.props.clearData()
    this.props.signOutAction()
  }
  groupLinks(){
      if (this.props.groups && this.props.groups.length > 0) {
        const groups = this.props.groups.map( (v,k) => {
          return (
            <NavLink to="" key={"group-"+k} className="nav-link">{v.name}</NavLink>
          )
        })
        return(
          <li className="nav-item dropdown" key="dropdown">
            <Link className="nav-link dropdown-toggle" to="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Your Groups
              </Link>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                {groups}
              </div>
          </li>
        )
      }
  }
  navbarLinks() {
    if (this.props.authenticated) {
      return [
        <li className="nav-item" key="board"><NavLink to="/board" className="nav-link" activeClassName="active">Board</NavLink></li>,
        <li className="nav-item dropdown" key="dropdown">
          <Link className="nav-link dropdown-toggle" to="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {this.props.username} {this.props.admin ? <span className="badge badge-secondary">Admin</span> : null}
            </Link>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
            <NavLink key="profile" to="/profile" className="nav-link" activeClassName="active">Profile</NavLink>
            <NavLink exact to="/admin" key="admin" className="nav-link" activeClassName="active">Admin</NavLink>
            <div className="dropdown-divider"></div>
            <Link className="nav-link" to="" key="logout" onClick={() => this.signOut()}>Log out</Link>
            </div>
        </li>

      ];
    }
    return [
      <li className="nav-item" key="login"><NavLink to="/login" className="nav-link" activeClassName="active">Login</NavLink></li>,
      <li className="nav-item" key="register"><NavLink to="/Register" className="nav-link" activeClassName="active">Register</NavLink></li>
    ];
  }
  render() {
    return (

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/tutorial" activeClassName="active">
                  Getting started
                  </NavLink>
                </li>
                {this.navbarLinks()}
                {this.groupLinks()}
              </ul>
          </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    username: state.auth.user.username,
    admin: state.auth.user.admin,
    groups: state.auth.user.groups,
    currentGroup: state.board.currentGroup
  };
}
export default connect(mapStateToProps, {signOutAction, clearData})(Navbar);
