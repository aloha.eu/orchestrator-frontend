import React, {Component} from 'react'
import  {Board} from 'react-trello'
import {RestService} from "../service/restService";
import {Tag} from 'react-trello'
import Results from "./results";
import Editor from './editor/index'
import CodeGen from './codegen'
import DeleteProject from './deleteproject'
import { ALOHA_FRONTEND_CONFIGURATION } from '../configuration/config.js';

import { connect } from 'react-redux'
import { setEventBus, setInitialLane, getBoardData, updateCard , updateProject, updateBoardData, deleteProject, startProjectExecution, popUpOpen, toggleEditor, loadingBoard} from "../redux/actions/board";
import {signOutAction} from "../redux/actions/auth";
import Toggle from 'react-toggle'
import "react-toggle/style.css"

import Moment from 'react-moment';
import moment from 'moment';


import  * as Icon from 'react-feather';
import Tippy from '@tippy.js/react';


const popperOptions = {positionFixed: true };

class MyBoard extends Component{
  componentDidMount(){
      this.props.loadingBoard(true)
      this.updateBoard()
  }

    //create an event bus to allow for altering of board from code
    //not currently used, but will be for preventing cards to be moved when they aren't ready
    setEventBus(handle){
      this.props.setEventBus({ // send eventBus to Store
          eventBus: handle
      })
    }

    //when a card is moved, send updated location to backend
    // todo: implement a check to see if card is allowed to move and if it can't,
    //  don't send update to backend and use event bus to move is back
    handleCardDrag(cardId, sourceLaneId, targetLaneId, position, cardDetails){
        if (sourceLaneId !== targetLaneId){
          cardDetails.label = "Ready";
        }
        this.props.updateCard({cardId, sourceLaneId, targetLaneId, position,cardDetails});

    }

    updateBoard()
    {
        if(this.props.user.user_id){
          try {
              this.props.getBoardData({userId: this.props.user.user_id});
          } catch(e) {
              console.log(e)
          }
        }else{
          this.props.signOutAction()
        }

    }

    forceUpdateBoard() {
        console.log("-- Board refresh --");
        this.updateBoard();
    }

    async downloadResult(cardId, projectId){

        this.openModal();
        //this.props.alert.info("Generating results file")
    }
    handleClickTB(cardId,laneId, projectId) { //Opens tensorboard

       window.open(ALOHA_FRONTEND_CONFIGURATION['tensorboardUrl'] + "#scalars&regexInput="+projectId + "&_smoothingWeight=0");
    }
    moveCardToLaneForwards(cardId, sourceLaneId){
      let cardDetails = {}
      const laneIndex = this.props.data.lanes.findIndex(lane => lane.id === sourceLaneId )
      cardDetails = this.props.data.lanes[laneIndex].cards.find(card => card.id === cardId)
      //let newLaneIndex = laneIndex.filter(value => value !== undefined)+1
      let newLaneIndex = laneIndex+1
      if(newLaneIndex === 3) {
        cardDetails.label = "Finished";
      }else{
        cardDetails.label = "Ready";
      }
      this.moveCard(cardId,sourceLaneId, newLaneIndex, cardDetails)
    }
    moveCardToLaneBackwards(cardId,sourceLaneId, label){
      let cardDetails = {}
      const laneIndex = this.props.data.lanes.findIndex(lane => lane.id === sourceLaneId )
      cardDetails = this.props.data.lanes[laneIndex].cards.find(card => card.id === cardId)
      let newLaneIndex = laneIndex-1
      if(newLaneIndex === 1) {
          cardDetails.label = "SL_Finished";
      }else if(newLaneIndex === 2){
          cardDetails.label = "Codegen_Finished";
      }else{
          cardDetails.label = "Finished";
      }
      //console.log(cardDetails.label)
      this.moveCard(cardId,sourceLaneId, newLaneIndex, cardDetails)
    }
    moveCard(cardId,sourceLaneId, newLaneIndex, cardDetails){
      let targetLaneId = this.props.data.lanes[newLaneIndex].id
      let position = 0
      this.props.updateCard({cardId, sourceLaneId, targetLaneId, position, cardDetails})
    }

    onCreate = tippy => {
      this.tippy = tippy
    }

    closeTippy = () => {
      this.tippy.hide()
    }
    cardNav(props, proj){
       const loc = "nav"
        if(this.whichLane(props.laneId) === 0){
          return(
          <div className="cardNav d-flex justify-content-between">
          <Tippy content="Edit Project"><button className="btn btn-info btn-sm" onClick={() => this.props.toggleEditor({ opened: this.props.editor.opened === true ? false : true, project: proj})}><Icon.Edit3 size="20" /></button></Tippy>
          <Tippy content="Open Tensorboard"><button className="btn btn-sm btn-default tensorboard_logo"
            onClick={() => this.handleClickTB(props.id, props.laneId, props.projectId)}></button></Tippy>
          <button className="btn btn-sm btn-default"
            onClick={() => this.moveCardToLaneForwards(props.id, props.laneId)} disabled={props.label === "Finished" && proj.model ? false : true}> Move to ▶︎ </button>
            <Tippy
              content={ <DeleteProject {...props} closeTip={this.closeTippy} deleteProject={this.props.deleteProject} />}
              placement="right-end" trigger="click" theme="light" interactive={true} inertia={true} arrow={false}  duration={[350, 200]}  popperOptions={popperOptions} onCreate={this.onCreate}>
              <button className="btn btn-sm float-right"><Icon.Trash2 size='20'/></button>
            </Tippy>
          </div>
        )
        }else{
          return(
            <div className="cardNav d-flex justify-content-between">
                {props.label === "SL_Finished" ? '' :
                <Results project={proj} model={proj.model} loc={loc} lane={this.whichLane(props.laneId)} updateProject={this.props.updateProject}></Results>
                }
                <Tippy content="Open Tensorboard"><button className="btn btn-sm btn-default tensorboard_logo"
                  onClick={() => this.handleClickTB(props.id, props.laneId, props.projectId)}></button></Tippy>
              <button className="btn btn-sm btn-default" disabled={props.label === "Running" ? true : false}
                onClick={() => this.moveCardToLaneBackwards(props.id, props.laneId)}>◀︎ Back</button>
              <button className="btn btn-sm btn-default" disabled={props.label === "Running" ? true : false}
                onClick={() => this.moveCardToLaneForwards(props.id, props.laneId)}> Next ▶︎</button>
                <Tippy
                  content={ <DeleteProject {...props} closeTip={this.closeTippy} deleteProject={this.props.deleteProject} />}
                  placement="right-end" trigger="click" theme="light" interactive={true} inertia={true} arrow={false}  duration={[350, 200]}  popperOptions={popperOptions} onCreate={this.onCreate}>
                  <button className="btn btn-sm float-right"><Icon.Trash2 size='20'/></button>
                </Tippy>
            </div>
          )
        }

    }
    handleToggleChange(event) {
      //console.log(event.target.dataset.proj)
      this.props.updateProject({projectId: event.target.dataset.proj, property: event.target.name, value: event.target.checked})
    }
    algorithmsCalc(proj){
      const algo_length = Object.keys(proj.models).length
      if(algo_length > 0){
        let diff = 0
        let calcdiff = 0
        proj.models.forEach( (k, v) =>{
          const creation =moment(k.creation_date.$date)
          const modified =moment(k.modified_date.$date)
          calcdiff = calcdiff + modified.diff(creation)
        })
        diff = calcdiff/algo_length

        const last_train = algo_length > 0 && proj.models[algo_length-1].modified_date.$date
        return(
          <div className="tra_data">
            <div className="tra_data-item">{algo_length} models trained</div>
            <div className="tra_data-item">
            {algo_length > 1? moment.duration(diff).format("mm:s")+ ' min/model trained': "Waiting to finish first train" }
            </div>
            <div className="tra_data-item">Last train finished <Moment fromNow>{last_train}</Moment></div>
         </div>
        )
      }else{
        return null
      }
    }
    projectOwnership(proj){
      let c = null
      if(proj.creator !== undefined){
       c = this.props.users.find(creator => creator._id.$oid = proj.creator.$oid)
      }
      return (
          <div>
          <p> <strong>Creator</strong> {c !== null ? c.username : "unknown"}</p>
          <p><strong>Groups</strong></p>
          {proj.groups && proj.groups.lenght > 0 ? proj.groups.map( (v, k) =>{
            const g = this.props.groups.find(group => group._id.$oid === v.$oid)
            return (<div key={k}>{g.name}</div>)
          })
         : "No groups assigned"}
        </div>
      )
    }
    cardContent(props, proj){
        const loc = "content"
        if(this.whichLane(props.laneId) === 0 && props.label === "Finished"){
            return(
            <div className="card-details">
            {proj.model && proj.model !== null ?
              <div className="model-selected mt-1 mb-2"> <span>Algorithm selected ID</span>{proj.model}</div>
              :
              <div className="alert alert-danger">Select an algorithm from <br/>results before continue.</div>
            }
            <div className="d-flex justify-content-between">
              <Results project={proj} popUpOpen={this.props.popUpOpen} loc={loc} lane={this.whichLane(props.laneId)} updateProject={this.props.updateProject}></Results>

              </div>
            </div>
            )
          }else  if (this.whichLane(props.laneId) === 0) {
            return(
            <div className="card-details">
              <div className="toggle-group form-group row">
                <label htmlFor="perf_en" className="col-sm-5 col-form-label col-form-label-sm">Performance</label>
                <div className="col-sm-4">{proj.per_en.status}</div>
                <div className="col-sm-3">
                <Toggle
                  data-proj={proj.id}
                  checked={proj.per_en.active}
                  disabled={true}
                  name='per_en'
                  onChange={this.handleToggleChange.bind(this)}
                  icons={false} />
                  </div>
              </div>
              <div className="toggle-group form-group row">
                <label htmlFor="tra_en" className="col-sm-5 col-form-label col-form-label-sm">Training  <Tippy content={props.label !== "Ready" ? this.algorithmsCalc(proj) : "Waiting for start"}><span tabIndex="0"><Icon.Info size="18" /></span></Tippy></label>
                <div className="col-sm-4 text-right">{proj.tra_en.status} </div>
                <div className="col-sm-3">
                <Toggle
                  data-proj={proj.id}
                  checked={proj.per_en.active}
                  disabled={true}
                  name='per_en'
                  onChange={this.handleToggleChange.bind(this)}
                  icons={false} />
                </div>
              </div>
            <div className="toggle-group form-group row">
              <label htmlFor="sec_en" className="col-sm-5 col-form-label col-form-label-sm">Security</label>
              <div className="col-sm-4">{proj.sec_en.status}</div>
              <div className="col-sm-3">
                <Toggle
                  checked={proj.sec_en.active}
                  data-proj={proj.id}
                  disabled={props.label === "Ready" ? false : true}
                  name='sec_en'
                  onChange={this.handleToggleChange.bind(this)}
                  icons={false} />
                </div>
              </div>
              <div className="toggle-group form-group row">
                <label htmlFor="rpi_en" className="col-sm-5 col-form-label col-form-label-sm">Parsimonious</label>
                <div className="col-sm-4">{proj.rpi_en.status}</div>
                <div className="col-sm-3">
                <Toggle
                  checked={proj.rpi_en.active}
                  data-proj={proj.id}
                  disabled={props.label === "Ready" ? false : true}
                  name='rpi_en'
                  onChange={this.handleToggleChange.bind(this)}
                  icons={false} />
                </div>
            </div>
            <div className="d-flex justify-content-between mt-2">
              { props.label === "Ready" ? <button className="btn btn-sm btn-primary"
                  onClick={() => this.handleClick2(props.id, props.laneId, props.projectId)}>Start
              </button> : null }

            </div>
            </div>
          )
        }else if(this.whichLane(props.laneId) === 1 && props.label === "SL_Finished"){
            return(
            <div className="card-details">
            <div className="toggle-group form-group row">
              <label htmlFor="sesame_en" className="col-sm-9 col-form-label col-form-label-sm">SESAME</label>
              <div className="col-sm-3">
              <Toggle
                checked={proj.sesame_en}
                data-proj={proj.id}
                disabled={true}
                name='sesame_en'
                onChange={this.handleToggleChange.bind(this)}
                icons={false}
                 />
                </div>
            </div>
            <div className="toggle-group form-group row">
              <label htmlFor="PI_en" className="col-sm-9 col-form-label col-form-label-sm">PI</label>
              <div className="col-sm-3">
              <Toggle
                checked={proj.PI_en}
                data-proj={proj.id}
                disabled={true}
                name='PI_en'
                onChange={this.handleToggleChange.bind(this)}
                icons={false} />
                </div>
          </div>
            <div className="d-flex justify-content-between mt-2">
              <Results project={proj} popUpOpen={this.props.popUpOpen} loc={loc} lane={this.whichLane(props.laneId)} updateProject={this.props.updateProject}></Results>

            </div>
            </div>
          )
        }else if (this.whichLane(props.laneId) === 1 && props.label === "Ready") {return(
          <div className="card-details">
            <div className="toggle-group form-group row">
              <label htmlFor="sesame_en" className="col-sm-9 col-form-label col-form-label-sm">SESAME</label>
              <div className="col-sm-3">
              <Toggle
                checked={proj.sesame_en}
                data-proj={proj.id}
                disabled={props.label === "Ready" ? false : true}
                name='sesame_en'
                onChange={this.handleToggleChange.bind(this)}
                icons={false}
                 />
                </div>
            </div>
            <div className="toggle-group form-group row">
              <label htmlFor="PI_en" className="col-sm-9 col-form-label col-form-label-sm">PI</label>
              <div className="col-sm-3">
              <Toggle
                checked={proj.PI_en}
                data-proj={proj.id}
                disabled={props.label === "Ready" ? false : true}
                name='PI_en'
                onChange={this.handleToggleChange.bind(this)}
                icons={false} />
                </div>
          </div>
          <div className="model-selected mt-1 mb-2"> <span>Algorithm selected ID</span> {proj.model === null ? 'Select from results' : proj.model }</div>
          <div className="control-group d-flex justify-content-between">
            <button className="btn btn-sm btn-primary" disabled={proj.model !== null ? false: true}
                onClick={() => this.handleClick2(props.id, props.laneId, props.projectId)}>{props.label === "Running" ? 'Stop' : 'Start'}
            </button>

          </div>
          </div>
        )
      }else if (this.whichLane(props.laneId) === 1 && props.label === "Running") {
            return  <div className="alert alert-info">SL is running right now.</div>;
      }else if(this.whichLane(props.laneId) === 2){
        return(<div className="card-details">
                    <div className="model-selected"> <span>Algorithm selected ID</span> {proj.model === null ? 'Select from results' : proj.model }</div>
                    <CodeGen proj={proj} props={props} />
              </div>)
      }else if(this.whichLane(props.laneId) === 3){
        return (<div className="card-details">
                    <div className="model-selected"> <span>Algorithm selected ID</span> {proj.model === null ? 'Select from results' : proj.model }</div>
                    <CodeGen proj={proj} props={props} />
              </div>
        )
      }else{
        return "Nothing to see";
      }

    }

    whichLane(laneId){
       const lane = this.props.data.lanes.find(lane => lane.id === laneId)
       return lane.position
    }
    showDates(proj){
      return(
        <div className="text-muted mb-1">
          <div><small>Last modified <Moment fromNow>{proj.modified_date.$date}</Moment></small></div>
        </div>
      )
    }
    async handleClick2(cardId,laneId, projectId) { // Start the process

            const response = await RestService.getCardById(cardId);
            let cardDetails = response.data;
            //console.log(cardDetails);

            let newStatus = "Running";

            if (cardDetails.label === "Running"){
                newStatus = "Stopped"
            }else if(cardDetails.label !== "Running" && cardDetails.label !== "Finished"){
                try {
                    const resp = this.props.startProjectExecution({projectId, cardDetails, newStatus});
                    console.log(resp);

                } catch(e){
                    console.log(e)
                    return
                }

            }
    }


    //display board
    render() {

        const CustomCard = (props)=> {
          const proj = this.props.projects.find(project => project.id === props.projectId)
          if(proj){
            return (
              <div style={{padding: 6}}>
                <header className="border-bottom border-light row board-card-header">
                  <h4 className="col-sm-6 pt-1 pl-4 board-card-header-title">
                  <Tippy content={this.projectOwnership(proj)}><span tabIndex="0">{props.title}</span></Tippy>
                  </h4>
                  <div className="col-sm-6 text-right text-muted ">
                    <div className="pr-2 board-card-header-details">
                    <strong>{props.label.toUpperCase()}</strong>
                    </div>
                    <div className="pr-2 board-card-header-details">{proj && moment(proj.modified_date.$date).fromNow()}</div>
                  </div>
                </header>
                <div>
                  {/* props.tags &&
                    <div
                      style={{
                        borderTop: '1px solid #eee',
                        paddingTop: 6,
                        display: 'flex',
                        justifyContent: 'flex-end',
                        flexDirection: 'row',
                        flexWrap: 'wrap'
                      }}
                    >
                      {props.tags.map(tag => <Tag key={tag.title} {...tag} tagStyle={props.tagStyle} />)}
                    </div> */}
                    <div>
                        {this.cardContent(props, proj)}
                    </div>
                    <div>
                        {this.cardNav(props, proj)}
                    </div>

                </div>

              </div>
            )
          }else{
            return null
          }

        };


        if(this.props.loading){
          return (<div className="text-center mt-4">
                    <div className="spinner-border text-primary" role="status">
                      <span className="sr-only">Loading...</span>
                    </div>
                    <p className="pt-2"><b>Wait!</b> Board is loading.</p>
                  </div>)
        }else{
          return (
          <div>
              <Editor title='Editor' opened={this.props.editor.opened} project={this.props.editor.project} presets={this.props.presets} groups={this.props.groups} >
              </Editor>
              <Board className="board" //data={data_dummy}
                   data={this.props.data}
                   draggable={false}
                   customCardLayout
                   laneDraggable={false}
                   eventBusHandle={this.setEventBus.bind(this)}
                   handleDragEnd={this.handleCardDrag.bind(this)}
                   //onDataChange={this.handleDataChange.bind(this)}
                   //onCardClick={this.handleCardClick.bind(this)}
                   >
                   <CustomCard key={this.props.id}/>

                   </Board>


          </div>
        )
      }
    }
}

// sagas communications
const mapStateToProps = state => {
  return {data: state.board.data,  projects: state.board.projects, eventBus : state.board.eventBus, initialLane: state.board.initialLane, editor: state.board.editor, presets: state.board.presets, users: state.board.users, groups:  state.auth.user.groups,user: state.auth.user, loading: state.board.loading};
};
function mapDispatchToProps(dispatch) {
  return {
    loadingBoard: data => dispatch(loadingBoard(data)),
    getBoardData: data => dispatch(getBoardData(data)),
    setInitialLane: data => dispatch(setInitialLane(data)),
    setEventBus: eventBus => dispatch(setEventBus(eventBus)),
    updateCard: cardDetails => dispatch(updateCard(cardDetails)),
    updateBoardData: newData => dispatch(updateBoardData(newData)),
    deleteProject: projectId => dispatch(deleteProject(projectId)),
    startProjectExecution: projectId => dispatch(startProjectExecution(projectId)),
    updateProject: data => dispatch(updateProject(data)),
    popUpOpen: data => dispatch(popUpOpen(data)),
    toggleEditor: data => dispatch(toggleEditor(data)),
    signOutAction: () => dispatch(signOutAction())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyBoard);
