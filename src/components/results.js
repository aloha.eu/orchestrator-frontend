import React, {Component} from 'react'
import PropTypes from 'prop-types'; // ES6
import '../assets/index.css';
import TrainResults from './trainresults';
import ProjectInfo from './projectinfo';
import Popup from "reactjs-popup";
import {Activity} from 'react-feather';
class Results extends Component {

    constructor(props) {
        super(props);
        this.state = {
          popUpOpened: false
        }
    }

    openModal(){
      this.setState({ // set popup status
          popUpOpened: true
      })
    }

    closeModal() {
      this.setState({ // set popup status
          popUpOpened: false
      })
    }

    render() {
        return (
            <Popup className="scrollable scrollCony"
                   trigger={
                       <button className={this.props.loc === "content" ? "btn btn-sm btn-primary"  : "btn btn-sm btn-info" }id={"triggerButton"}> {this.props.loc === "content" ? "View results" : <Activity size="20" />}</button>
                   } modal
                   closeOnEscape
                   open={this.props.popUpOpened}
                   onClose={this.closeModal.bind(this)}
                   onOpen={this.openModal.bind(this)}
                   contentStyle={modalStyle}>
                   {this.props.lane === 0 ? <TrainResults project={this.props.project} model={this.props.model} updateProject={this.props.updateProject}></TrainResults>
                   :
                   <ProjectInfo project={this.props.project} model={this.props.model} updateProject={this.props.updateProject}></ProjectInfo>
                  }

            </Popup>


    )
  }
}
const modalStyle = {width:"95%", height: "95%"}
Results.propTypes={
    updateProject: PropTypes.func,
}
export default Results;
