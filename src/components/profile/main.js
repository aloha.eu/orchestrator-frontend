import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"
import {connect} from 'react-redux'
import { signOutAction } from '../../redux/actions/auth';


class Main extends Component {
  constructor(props){
    super(props);

    this.state = {
      user: {
        id: {},
        name: '',
        surname: '',
        email: '',
        username: '',
        modified_date: {},
        creation_date: {},
        active: true,
        admin: false
      },
      message: '',
      status: ''
    }
    this.handleSubmitUser = this.handleSubmitUser.bind(this);
    this.handleChangeUser = this.handleChangeUser.bind(this);
  }
  componentDidMount(){
  this.getUser()
  }
  async getUser(){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.getUser(this.props.user_id).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({user: response.data.user})
    })

  }
  async updateUser(formData){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.updateUser(this.props.user_id, formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({user: response.data.user})
    })
  }

  handleSubmitUser(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('name', this.state.user.name);
    formData.append('surname', this.state.user.surname);
    formData.append('username', this.state.user.username);
    formData.append('email', this.state.user.email);
    this.updateUser(formData)
  }

  handleChangeUser(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({ user:
      { ...this.state.user,
        [name]: value
      }
    });
    //console.log(`Changing: ${value} in ${name}`)
  }


  render() {
    return (
      <div className="col-md-9 ml-sm-auto col-lg-10 px-4">

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h2 className="h2">General Profile</h2>
        </div>
            <div className="d-flex">{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>

            <form onSubmit={ this.handleSubmitUser }>
                <div className="mb-4">
                <div className="form-label-group">
                <input name="username"
                      type="text"
                      className="form-control"
                      value={this.state.user.username}
                      onChange={this.handleChangeUser}  />
                <label htmlFor="username">Username</label>
                </div>

                <div className="form-row">
                  <div className="form-label-group col">
                      <input name="name"
                            type="text"
                            className="form-control"
                            value={this.state.user.name}
                            onChange={this.handleChangeUser}  />
                  <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-label-group col">
                  <input name="surname"
                        type="text"
                        className="form-control"
                        value={this.state.user.surname}
                        onChange={this.handleChangeUser}  />
                  <label htmlFor="surname">Surname</label>
                  </div>
                </div>
                <div className="form-label-group">
                <input name="email"
                      type="text"
                      className="form-control"
                      value={this.state.user.email}
                      onChange={this.handleChangeUser}  />
                <label htmlFor="email">Email</label>
                </div>


                  <button type="submit" className="btn btn-primary">Submit changes</button>
                </div>
                </form>

      </div>


    );
    }
}
const mapStateToProps = state => {
  return {user_id: state.auth.user.user_id,  username: state.auth.user.username};
};
export default connect(mapStateToProps, {signOutAction})(Main)
