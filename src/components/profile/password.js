import React, { Component} from 'react'
import {RestService} from "../../service/restService";
import Utils from "../../utils"
import {connect} from 'react-redux'
import { signOutAction } from '../../redux/actions/auth';


class Password extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {
        id: {},
        name: '',
        surname: '',
        email: '',
        username: '',
        modified_date: {},
        creation_date: {},
        active: true,
        admin: false
      },
      password: '',
      password_ver: '',
      message: '',
      status: ''
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){

  this.getUser()
  }
  async getUser(){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.getUser(this.props.user_id).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      this.setState({user: response.data.user})
    })

  }
  async changePassword(formData){
      this.setState({message: 'loading...', status: 'loading'})
    await RestService.changePassword(formData).then(response => {
      this.setState({message: response.data.message, status: response.data.status})
      if(response.data.status === 'success'){
        alert(response.data.message);
        setTimeout(() =>{ this.props.signOutAction()}, 3000);
      }
    })

  }
  handleSubmit(event){
    event.preventDefault();
    let formData = new FormData(event.target);
    formData.append('password', this.state.password);
    formData.append('password_ver', this.state.password_ver);
    formData.append('email', this.state.user.email);
    this.changePassword(formData)
  }
  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
        [name]: value
    });
  }

  render() {
    return (
      <div className="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h2 className="h2">Change Password</h2>
        </div>
        <div className="alert alert-warning"><b>Atention:</b> By changing your password you will be logged off.</div>

            <div className="d-flex">{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>

                <form onSubmit={ this.handleSubmit }>
                    <div className="mb-4">
                    <div className="form-label-group">
                        <input name="password"
                              type="password"
                              className="form-control"
                              value={this.state.password}
                              onChange={this.handleChange}  />
                    <label htmlFor="password">Password</label>
                    </div>
                    <div className="form-label-group">
                    <input name="password_ver"
                          type="password"
                          className="form-control"
                          value={this.state.password_ver}
                          onChange={this.handleChange}  />
                    <label htmlFor="password_ver">Repeat new password</label>
                    </div>


                    <input name="email"
                          type="hidden"
                          className="form-control"
                          value={this.state.user.email} />
                      <button type="submit" className="btn btn-primary">Change password</button>
                    </div>

                    </form>

      </div>


    );
    }
}
const mapStateToProps = state => {
  return {user_id: state.auth.user.user_id,  username: state.auth.user.username};
};
export default connect(mapStateToProps, {signOutAction})(Password)
