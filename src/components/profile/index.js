import React from 'react'
import { Route, useRouteMatch, useParams, Switch, NavLink } from 'react-router-dom';
import { connect } from 'react-redux'
import Moment from 'react-moment';
import '../../assets/admin.css'
import Main from './main'
import Password from './password'

const Profile = ({ user }) => {
    let { path, url } = useRouteMatch();
    return (
      <div>
        <nav className="col-md-2 bg-light sidebar">

        <div className="sidebar-sticky">
          <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Profile</span>
          </h6>
          <ul className="nav flex-column">
            <li className="nav-item"><NavLink exact to={`${url}`} className={"nav-link"} activeClassName="active">General</NavLink></li>
            <li className="nav-item"><NavLink exact to={`${url}/password`} className="nav-link" activeClassName="active">Change Password</NavLink></li>
          </ul>
          <ul className="nav flex-column mt-4">
            <li className="nav-item px-3 mb-1 text-muted"><small>Active: {user.active ? 'Yes' : 'No'}</small></li>
            <li className="nav-item px-3 mb-1 text-muted"><small>Admin: {user.admin ? 'Yes' : 'No'}</small></li>
            <li className="nav-item px-3 mb-1 text-muted"><small>Created: <Moment format="DD-mm-YYYY mm:ss">{user.creation_date ? user.creation_date.$date : null}</Moment></small></li>
            <li className="nav-item px-3 mb-1 text-muted"><small>Modified: <Moment format="DD-mm-YYYY mm:ss">{user.modified_date ? user.modified_date.$date : null}</Moment></small></li>
          </ul>
        </div>
        </nav>
              <Switch>
              <Route exact path={path} url={url} component={Main} />
              <Route path={`${path}/:topicId`}>
                <AdminRoutes {...user} />
              </Route>
              </Switch>
      </div>
    );
}
const AdminRoutes = ({ user }) => {
  // The <Route> that rendered this component has a
  // path of `/topics/:topicId`. The `:topicId` portion
  // of the URL indicates a placeholder that we can
  // get from `useParams()`.
  let { topicId } = useParams();
  switch (topicId) {
    case 'main':
      return (<Main user={user} />)
    case 'password':
      return (<Password user={user} />)
    default:
      return (<Main user={user} />)
  }
}

// sagas communications
const mapStateToProps = state => {
  return {user: state.auth.user};
};


export default connect(mapStateToProps)(Profile);
