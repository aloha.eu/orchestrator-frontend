import React, {Component} from 'react'
import PropTypes from 'prop-types'; // ES6
import {RestService} from "../service/restService";
import '../assets/index.css';
import Utils from '../utils'
import { ALOHA_FRONTEND_CONFIGURATION } from '../configuration/config.js';
import {
  ScatterChart, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip, ResponsiveContainer, Cell, BarChart, Bar, LineChart, Line, Legend
} from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
const colors = scaleOrdinal(schemeCategory10).range();
const bootstrapColors = {
    success: "#5cb85c",
    primary: "#0275d8",
    info: "#5bc0de",
    warning: "#f0ad4e",
    danger: "#d9534f",
    inverse: "#292b2c",
    faded:  "#f7f7f7"
  }
const sec_color  = { low: bootstrapColors.danger , medium: bootstrapColors.warning , high: bootstrapColors.success }

class TrainResults extends Component {

    constructor(props) {
        super(props);

        this.state = {
          popUpOpened: false,
          project_result:[],
          results:[],
            num_alg: 0,
          acc_type: 'validation'};
        this.handleChange = this.handleChange.bind(this)
    }
    componentDidMount(){
      this.loadResults()
    }
    async loadResults() {
        let project = await RestService.getProjectResult(this.props.project.id);

        this.setState({results: project.data})

    }
    datum(type){
      //console.log(this.state.results)
      let values = {}
      switch (type) {
        case 'accuracy':
          values = this.state.results.map( (r,k) =>{
              return (
                {index: k,
                x: this.state.acc_type === 'validation' ? r.validation_accuracy : r.training_accuracy,
                y: this.state.acc_type === 'validation' ? r.validation_loss : r.training_loss}
              )
            })
          return {data: values, labels: {x: 'Validation Accuracy', y: 'Validation Loss'}}
        case 'power_per':
          values = this.state.results.map( (r,k) =>{
              return (
                {index: k,
                x: r.performance,
                y: r.energy}
              )
            })
          return {data: values, labels: {x: 'Performance', y: 'Energy'}}

        case 'security':
          values = this.state.results.map( (r,k) =>{
              return (
                {index: k,
                x: r.sec_level}
              )
            })
          return {data: values, labels: {x: 'Security Level'}}

        case 'rpi':
        values = this.state.results.map( (r,k) =>{
            let rpi = Utils.toObject(r.rpi_best_quantization)
            if(r.rpi_best_quantization !== undefined){
              return (
                {index: k,
                x: rpi.rpi_training_accuracy,
                y: rpi.rpi_training_loss,
                i: rpi.index}
              )
            }else{
              return (
                {index: k,
                x: null,
                y: null,
                i: null}
              )
            }

          })
          return  {data: values, labels: {x: 'Training Accuracy', y: 'Training Loss', i: 'Index'}}

        case 'plot_a':
          values = this.state.results.map( (r,k) =>{
              let rpi = Utils.toObject(r.rpi_best_quantization)
              let size = 4
              if(r.rpi_best_quantization !== undefined){
                if(Object.keys(rpi).length > 0){
                  size = rpi.rpi_quantization[1]
                }
                return (
                  {index: k,
                  x: r.performance,
                  y: r.validation_accuracy,
                  sec_level : r.sec_level,
                  size: size
                  }
                )
              }else{
                return (
                  {index: k, x: null, y: null, sec_level: null, size: size}
                )
              }
            })
          return  {data: values, labels: {x: 'Execution time [ms]', y: 'Validation Accuracy'}}

        case 'plot_b':
          values = this.state.results.map( (r,k) =>{
              let rpi = Utils.toObject(r.rpi_best_quantization)
              let size = 4
              if(r.rpi_best_quantization !== undefined){
                if(Object.keys(rpi).length > 0){
                  size = rpi.rpi_quantization[1]
                }
                return (
                  {index: k,
                  x: r.energy,
                  y: r.validation_accuracy,
                  sec_level : r.sec_level,
                  size: size
                  }
                )
              }else{
                return (
                  {index: k, x: null, y: null, sec_level: null, size: size}
                )
              }
            })
          return  {data: values, labels: {x: 'Energy [J]', y: 'Validation Accuracy'}}

          case 'plot_c':
            values = this.state.results.map( (r,k) =>{

                let rpi = Utils.toObject(r.rpi_best_quantization)
                let size = 4
                if(r.rpi_best_quantization !== undefined){
                  if(Object.keys(rpi).length > 0){
                    size = rpi.rpi_quantization[0]
                  }
                  return (
                    {index: k,
                    x: r.memory,
                    y: r.validation_accuracy,
                    sec_level : r.sec_level,
                    size: size
                    }
                  )
                }else{
                  return (
                    {index: k, x: null, y: null, sec_level: null, size: size}
                  )
                }
              })
            return  {data: values, labels: {x: 'Memory [B]', y: 'Validation Accuracy'}}

        default:
          return values
      }

    }
    handleOver = (data, index) => {
      const tr = document.getElementById(`algo-${data.payload.index}`);
      tr.classList.add('focus')
    }
    handleOut = (data, index) => {
      const tr = document.getElementById(`algo-${data.payload.index}`);
      tr.classList.remove('focus')
    }
    lineChart(values){
      return(
        <ResponsiveContainer minWidth={150} height={200}>
        <LineChart width={400} height={200}
        data={values.data}>
          <CartesianGrid />
            <XAxis dataKey="index" />
            <YAxis  />
            <Tooltip content={<CustomTooltip />} />
            <Legend />
            <Line type="monotone" dataKey="x" stroke="#8884d8" name={values.labels.x} />
            <Line type="monotone" dataKey="y" stroke="#82ca9d" name={values.labels.y} />
          </LineChart>
        </ResponsiveContainer>
      )
    }
    scatterChart(values){
      //console.log(values)
      return(
        <ResponsiveContainer minWidth={150} height={200}>
          <ScatterChart  width={400}height={200} >
            <CartesianGrid />
            <XAxis type="number" dataKey="x" name={values.labels.x} />
            <YAxis type="number" dataKey="y" name={values.labels.y} />
            <Tooltip content={<CustomTooltip />} />
            <Scatter name="A school" data={values.data} fill="#8884d8" onMouseOver={this.handleOver.bind(this)} onMouseOut={this.handleOut.bind(this)}>
            {
              values.data.map((entry, index) => <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />)
            }
            </Scatter>
          </ScatterChart>
        </ResponsiveContainer>
      )
    }
    scatterChartSec(values){
      //console.log(values)
      return(
        <ResponsiveContainer minWidth={150} height={200}>
          <ScatterChart  width={400}height={200} >
            <CartesianGrid />
            <XAxis type="number" dataKey="x" name={values.labels.x}  label={{ value: values.labels.x, offset:0, position:"insideBottom" }} />
            <YAxis type="number" dataKey="y" name={values.labels.y} label={{ value: values.labels.y, angle: -90 }} />
            <ZAxis type="number" dataKey="size" range={[30, 100]} name="size" />
            <Tooltip content={<CustomTooltip />} />
            <Scatter data={values.data} fill="#8884d8" onMouseOver={this.handleOver.bind(this)} onMouseOut={this.handleOut.bind(this)}>
            {
              values.data.map((entry, index) => <Cell key={`cell-${index}`} fill={sec_color[entry.sec_level]} />)
            }
            </Scatter>
          </ScatterChart>
        </ResponsiveContainer>
      )
    }
    barChart(values){
    //  console.log(datum)
      return(
        <ResponsiveContainer minWidth={150} minHeight={150}>
          <BarChart data={values.data}  width={400}height={200} syncId="syncId"  >
            <CartesianGrid strokeDasharray="3 3" />
            <YAxis yAxisId="left" orientation="left" stroke="#8884d8" />
            <YAxis yAxisId="right" orientation="right" stroke="#82ca9d" />
            <Tooltip content={<CustomTooltip />} />
              <Bar yAxisId="left" dataKey="x" name={values.labels.x} fill="#8884d8"  onMouseOver={this.handleOver.bind(this)} onMouseOut={this.handleOut.bind(this)} />
              <Bar yAxisId="right" dataKey="y" name={values.labels.y} fill="#82ca9d" onMouseOver={this.handleOver.bind(this)} onMouseOut={this.handleOut.bind(this)} />
          </BarChart>
        </ResponsiveContainer>
      )
    }
    table(data) {
      let row = this.state.results.map( (r,k) =>{

        return(
          <tr key={k} id={`algo-${k}`}>
            <td id={r.id} title={r.id}>{k}</td>
            <td>{r.training_loss}</td>
            <td>{r.training_accuracy}</td>
            <td>{r.validation_loss}</td>
            <td>{r.validation_accuracy}</td>
            <td>{r.sec_level}</td>
            <td>{r.performance}</td>
            <td>{r.energy}</td>
            <td>{r.processors}</td>
            <td>{r.memory}</td>
            <td>{r.rpi_training_accuracy ? Utils.toObject(r.rpi_training_accuracy).map((item,key) => <div key={key} className={Utils.toObject(r.rpi_best_quantization).index === key ? "text-info" : ""}>{item}</div>) : null}</td>
            <td>{r.rpi_training_loss ? Utils.toObject(r.rpi_training_loss).map((item,key) => <div key={key} className={Utils.toObject(r.rpi_best_quantization).index === key ? "text-info" : ""}>{item}</div>) : null}</td>
            <td>{r.rpi_quantization ? Utils.toObject(r.rpi_quantization).map((item,key) => <div key={key} className={Utils.toObject(r.rpi_best_quantization).index === key ? "text-info" : ""}>{Utils.toTuples(item)}</div>) : null}</td>
            <td><button onClick={() => this.downloadImage(r.rpi_img_path)} disabled={r.rpi_img_path === undefined ? true : false } className={"btn btn-sm btn-info"}>↓</button></td>
            <td>
              <a onClick={() => RestService.openNetron(r.onnx)}
                 href={ALOHA_FRONTEND_CONFIGURATION['netronUrl'] + "?onnx_path="+r.onnx} target="_blank" rel="noopener noreferrer">
              {this.state.project_result.onnx}
              </a>
              <button onClick={() => this.downloadFile(r.onnx_trained)} className={"btn btn-sm btn-info"}>Download</button>
            </td>
            <td><button onClick={() => this.selectAlgo({projectId: this.props.project.id, property: "model", value:r.id})} className={"btn btn-sm btn-success"} disabled={r.id === this.props.model ? true : false}>{r.id === this.props.model ? 'Model selected' : 'Select this model'}</button></td>
          </tr>
        )
      })
        return(
          <table className="table responsive results-list">
          <thead className="text-left">
            <tr>
              <th>Index</th>
              <th colSpan="2">Training Evaluation</th>
              <th colSpan="2">Validation Evaluation</th>
              <th>Security</th>
              <th colSpan="4">Power performance evaluation</th>
              <th colSpan="4">Refinement for Parsimonious Inference</th>
              <th></th>
              <th></th>
            </tr>
            <tr>
              <th></th>
              <th>Loss</th>
              <th>Accuracy</th>
              <th>Loss</th>
              <th>Accuracy</th>
              <th>Level</th>
              <th>Performance <span className="text-muted">[ms]</span></th>
              <th>Energy <span className="text-muted">[J]</span></th>
              <th>Processors <span className="text-muted">[#]</span></th>
              <th>Memory <span className="text-muted">[B]</span></th>
                <th>Accuracy</th>
                <th>Loss</th>
                <th>Quantization <span className="text-muted">[w_bits, x_bits]</span></th>
                <th>Plot</th>
              <th>ONNX File</th>
              <th></th>
            </tr>
            </thead>
          <tbody>

            {row}
          </tbody>
          </table>
        )
    }
    handleChange(event){
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      console.log(value)
      //console.log(name)
      this.setState({
        [name]: value
      });
  }
  selectAlgo(data){
    this.props.updateProject(data)
  //  console.log(data)
//    console.log(projectId)
//    this.closeModal()
  }
    downloadFile(filePath){
      //  this.props.alert.success("Downloading file")
        let response = RestService.getProjectFile(filePath);
        console.log(response)
    }

    downloadImage(imagePath){
    //    this.props.alert.success("Downloading results image")
        let response = RestService.getProjectFile(imagePath);
        console.log(response)
    }

    closeModal() {
      this.setState({ // set popup status
          popUpOpened: false
      })
    }

    render() {

       // var sf = "file:///home/nur/docker_arch/shared_data/"
        //var training_img_path = sf + this.state.project_result.training_img_path
        //var tensorboard_url = ALOHA_FRONTEND_CONFIGURATION['tensorboardUrl'] + "#scalars&regexInput="+this.props.project + "&_smoothingWeight=0"
        return (
            <div className="results container-fluid">

                  <div className="row"><p>Algorithm Configuration summary</p></div>
                <div className="row"><h5>{this.props.project.title} <small>(ID: {this.props.project.id})</small></h5></div>
                <div className="row">
                  <div className="alert alert-dark"><b>Tip:</b> Lower values in accuracy, in combination with energy, memory and time execution, are considered the optimal. It is suggested to look at the bottom-left quadrant as the zone of best performance.</div>
                </div>
              <div className="row">

                <div className="col-sm-4">
                  <h5>Execution time</h5>
                  {this.state.results.length > 0 ? this.scatterChartSec(this.datum('plot_a')) : "No data available yet."}
                </div>
                <div className="col-sm-4">
                  <h5>Energy consumption</h5>
                  {this.state.results.length > 0 ? this.scatterChartSec(this.datum('plot_b')) : "No data available yet."}
                </div>
                <div className="col-sm-4">
                  <h5>Memory footprint</h5>
                  {this.state.results.length > 0 ? this.scatterChartSec(this.datum('plot_c')) : "No data available yet."}
                </div>

              </div>
              <div className="row">
                <div className="col-sm-3">
                    <h5>Accuracy</h5>
                    <div>
                      {this.state.results.length > 0 ? this.barChart(this.datum('accuracy')) : "No data available yet."}
                    </div>
                    <div className="mb-3">
                      <label htmlFor="acc_type">Type of accuracy levels</label>
                      <select title="Accuracy selector"
                      id="acc_type" name="acc_type" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.acc_type}>
                          <option value="validation">Validation</option>
                          <option value="training">Training</option>

                      </select>
                    </div>
                </div>
                <div className="col-sm-3">
                  <h5>Power Performance</h5>
                  <div>
                    {this.state.results.length > 0 ? this.barChart(this.datum('power_per')) : "No data available."}
                  </div>
                </div>
                <div className="col-sm-3">
                  <h5>Security</h5>
                  <div>
                  {this.props.project.sec_en.active === true ? this.barChart(this.datum('security')) : "Security module is off. No data available."}
                  </div>
                </div>
                <div className="col-sm-3">
                  <h5>RPI Best Quantization</h5>
                  <div>
                  {this.state.results.length > 0 ? this.lineChart(this.datum('rpi')) : "No data available."}
                  </div>
                </div>
              </div>
              <div className="row">
                {this.state.results.length > 0 ? this.table() : 'No results to show'}
              </div>
            </div>


    )
  }
}
function CustomTooltip({ payload, label, active }) {
  //console.log(payload)
  if (active && payload[0] !== undefined) {
    return (
      <div className="custom-tooltip card">
        <div className="card-header">Algorithm {payload[0].payload.index}</div>
        <div className="card-body">
        <p>{`${payload[0].name} : ${payload[0].value}`}</p>
        {payload.length > 1 ?
          <p>{`${payload[1].name} : ${payload[1].value}`}</p>
          : null
        }
        {payload[0].payload.i >= 0 ?
          <p className="desc">RPI Index: {payload[0].payload.i}</p>
          : null
        }

        </div>
      </div>
    );
  }

  return null;
}
TrainResults.propTypes={
    updateProject: PropTypes.func,
}
export default TrainResults;
