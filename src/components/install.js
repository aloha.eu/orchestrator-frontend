import React, {Component} from 'react'
import { Link } from 'react-router-dom';
import {RestService} from "../service/restService";
import Utils from "../utils"

class Install extends Component {
  constructor(props) {
      super(props);
      this.state = { message: '', status: '', loaded: false};
  }
  componentDidMount(){
    this.installDependencies()
  }

  async installDependencies(){
        this.setState({message: 'Wait while we set up the database...', status: 'loading'})
    await RestService.install().then( response => {
      console.log(response.data)
        this.setState({message: response.data.message, status: response.data.status})
        if(response.status === 200){
          this.setState({loaded: true})
        }
    })
  }
  render(){
    return(
        <div className="text-center mb-4 mt-4">
        <h1>Welcome to Aloha project.</h1>
        <div>{this.state.message !== '' ? <div className={'alert alert-'+Utils.getColorCode(this.state.status)}>{this.state.message}</div> : null}</div>
        {this.state.loaded ? <Link to="/register" className="btn btn-primary">Continue to register</Link> : null }
      </div>
    )
  }
}
export default Install;
