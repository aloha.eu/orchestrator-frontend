import React, {Component} from 'react'
import { Link } from 'react-router-dom';


class Homepage extends Component {
  render(){
    return(
        <div>
        <div className="jumbotron">
        <div className="container">
          <h1 className="display-4">Welcome to ALOHA Toolflow!</h1>
          <p className="lead">This is part of the <a href="https://www.aloha-h2020.eu">ALOHA EU H2020 Project</a></p>
          <hr className="my-4" />
          <p>Please log in to continue.</p>
          <Link to="/login" className="btn btn-primary btn-lg">Sign in</Link>
        </div>
      </div>
      <div className="container"><small>For the first time, be sure to proceed with the <Link to="/install">install</Link> of the system.</small></div>
      </div>
    )
  }
}
export default Homepage;
