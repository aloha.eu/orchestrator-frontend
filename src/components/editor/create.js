import React, {Component} from 'react'
import {RestService} from "../../service/restService";
import {Link} from "react-router-dom"
import Toggle from 'react-toggle'
import { connect } from 'react-redux'
import { createProject, toggleEditor, editProject} from "../../redux/actions/board";
import Moment from 'react-moment';
import  * as Icon from 'react-feather';
import Tippy from '@tippy.js/react';

const initialState = {
  title:"New Project",
  description : "Description",
  groups: '',
  mode:"full",
  use_cases:"",
  data_set_name:"CIFAR10",
  data_folder:"",
  data_file:"",
  algo_folder:"algorithms/gridsearch",
  lr:"0.0001",
  lr_decay:"0.1",
  ref_steps:"4",
  ref_patience:"15",
  batch_size : "64",
  num_epochs:"5",
  loss:"softmax",
  optimizer:"sgd",
  image_width:32,
  image_height:32,
  image_number_of_channels:3,
  num_classes:"10",
  num_filters:"16",
  upconv:"upconv",
  nonlin:"elu",
  task_type:"classification",
  accuracy:"percent",
  augmentation_horizontal:"false",
  augmentation_vertical:"false",
  data_split:"0.7",
  normalize:"false",
  zero_center:"false",
  /* Constrains */
  power_value : 25,
  power_priority : 1,

  security_value : 2,
  security_priority : 1,

  execution_time_value : 1,
  execution_time_priority : 1,

  accuracy_value : 80.0,
  accuracy_priority : 1,

  /* architecture */
  architecture_name : "Architecture",
  architecture_path : "architectures/arch.json",

  sec_en : {active: true, status:'Ready'},
  tra_en : {active: true, status:'Ready'},
  rpi_en : {active: false, status:'Disabled'},
  per_en : {active: false, status:'Disabled'},
  sec_act: false,
  tra_act: true,
  rpi_act: false,
  per_act: true,
  sesame_en : convertOnOff("off"),
  PI_en : convertOnOff("off"),

  creation_date: '',
  modified_date: ''
}
class ProjectCreator extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeUsecase = this.handleChangeUsecase.bind(this);
        this.handleChangeGroups = this.handleChangeGroups.bind(this);
        this.fileInput = React.createRef();
    }
    async getProjectConfig(projectId){

      let response = await RestService.getProjectByIdEdit(projectId)
      response.data.per_act = response.data.per_en.active
      response.data.tra_act = response.data.tra_en.active
      response.data.sec_act = response.data.sec_en.active
      response.data.rpi_act = response.data.rpi_en.active
      response.data.image_width = response.data.image_dimensions[0]
      response.data.image_height = response.data.image_dimensions[1]
      response.data.image_number_of_channels = response.data.image_dimensions[2]
      response.data.augmentation_vertical = response.data.augmentation.flip_vert
      response.data.augmentation_horizontal = response.data.augmentation.flip_hor
      //console.log(response.data)
      this.setState(response.data)

    }

    componentDidMount(){
      this.props.toggleEditor({ // set popup status
          opened:true,
          project: this.props.project
      })
      if(this.props.project){
        this.getProjectConfig(this.props.project.id)
      }
    }

    closeModal() {
      console.log('close modal')
      this.props.toggleEditor({ // set popup status
          opened:false,
          project: null
      })

    }
    cancel(event){
      event.preventDefault();
      this.closeModal()
    }

    async uploadArchitecture(event){
      event.preventDefault();
      console.log(this.fileInput.current.files[0])
      if(this.fileInput.current.files[0]){
        const data = new FormData()
        data.append('file', this.fileInput.current.files[0])
    //    data.append('architecture_name', this.state.architecture_name);
        for (var pair of data.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }
        try{
          await RestService.uploadFile(data, 'architectures').then((response)=>{ this.setState({architecture_path: `architectures/${response.data}`}) })

        }catch(e){
          console.log(e)
        }

      }else{
          console.log("No file selected.")
      }

    }
    //function to handle the creation of a project
    //creates project in backend and uses that response to create a new card
    //todo: optimize this and have it close the form modal
    async handleSubmit(event){
        event.preventDefault();
         console.log(this.state.groups)
        //const configFile = configFile.files[0];
        // const algorithmConfig = algorithmConfigFile.files[0];
       // const architectureSpec = architectureSpecFile.files[0];
        let formData = new FormData(event.target);
        //formData.append('config', configFile);
        formData.append('title', this.state.title);
        formData.append('description', this.state.description);

        if(!this.props.project){
        //  this.state.groups.forEach(val => {
        //    formData.append('groups[]', val);
        //  });
          formData.append('groups', this.state.groups)
        }

        formData.append('mode', this.state.mode);
        formData.append('use_cases', this.state.use_cases);
        formData.append('data_set_name', this.state.data_set_name);
        formData.append('data_folder', this.state.data_folder);
        formData.append('data_file', this.state.data_file);
        formData.append('algo_folder', this.state.algo_folder);
        formData.append('lr', this.state.lr);
        formData.append('lr_decay', this.state.lr_decay);
        formData.append('ref_steps', this.state.ref_steps);
        formData.append('ref_patience', this.state.ref_patience);
        formData.append('batch_size', this.state.batch_size);
        formData.append('num_epochs', this.state.num_epochs);
        formData.append('loss', this.state.loss);
        formData.append('optimizer', this.state.optimizer);
        formData.append('image_width', this.state.image_width);
        formData.append('image_height', this.state.image_height);
        formData.append('image_number_of_channels', this.state.image_number_of_channels);
        formData.append('num_classes', this.state.num_classes);
        formData.append('num_filters', this.state.num_filters);
        formData.append('upconv', this.state.upconv);
        formData.append('nonlin', this.state.nonlin);
        formData.append('task_type', this.state.task_type);
        formData.append('accuracy', this.state.accuracy);
        formData.append('augmentation_horizontal', this.state.augmentation_horizontal);
        formData.append('augmentation_vertical', this.state.augmentation_vertical);
        formData.append('data_split', this.state.data_split);
        formData.append('normalize', this.state.normalize);
        formData.append('zero_center', this.state.zero_center);

        /* Constrains */
        formData.append('power_value', this.state.power_value);
        formData.append('power_priority', this.state.power_priority);

        formData.append('security_value', this.state.security_value);
        formData.append('security_priority', this.state.security_priority);

        formData.append('execution_time_value', this.state.execution_time_value);
        formData.append('execution_time_priority', this.state.execution_time_priority);

        formData.append('accuracy_value', this.state.accuracy_value);
        formData.append('accuracy_priority', this.state.accuracy_priority);

        formData.append('per_act', convertOnOff(this.state.per_act));
        formData.append('sec_act', convertOnOff(this.state.sec_act));
        formData.append('tra_act', convertOnOff(this.state.tra_act));
        formData.append('rpi_act', convertOnOff(this.state.rpi_act));

        formData.append('sesame_en', convertOnOff(this.state.sesame_en));
        formData.append('PI_en', convertOnOff(this.state.PI_en));


       formData.append('architecture_name', this.state.architecture_name);
        formData.append('architecture_path', this.state.architecture_path);


        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }
        if(this.props.project){
          this.props.editProject(formData, this.props.project.id)
        }else{
          this.props.createProject(formData)
        }

      this.closeModal()


    }

    showDates(){
      return(
        <div className="text-muted float-right mt-3">
          <small>Created: <Moment unix format="DD-mm-YYYY mm:ss">{this.state.creation_date.$date}</Moment> </small>
          <small>Modified: <Moment unix format="DD-mm-YYYY mm:ss">{this.state.modified_date.$date}</Moment></small>
        </div>
      )
    }
    presets(){
      const option = this.props.presets.map( (v,k) => {
        return(<option key={"preset-"+k} value={k}>{v.title}</option>)
      })
      return(
        <select title="Choose one of the preset use cases or select none for a full custom configuration"
        id="use_cases" name="use_cases" onChange={this.handleChangeUsecase} className="form-control form-control-sm" value={this.state.use_cases}>
            <option value="">None</option>
            {option}
        </select>

      )
    }
    groups(){
      const option = this.props.groups.map( (v,k) => {
        return(<option key={"group-"+k} value={v._id.$oid}>{v.name}</option>)
      })
      if(this.props.project){
       return(<div className="alert alert-info">Groups can be only changed in the <Link to ="/admin/groups">Admin</Link> section.</div>)
     }else{
       return(
         <div>
         <select title="Choose one of the preset use cases or select none for a full custom configuration"
         id="groups" name="groups" onChange={this.handleChangeGroups} className="form-control form-control-sm" value={this.state.groups}>
              <option key={"group-none"} value="" disabled="disabled">Select one group</option>
             {option}
         </select>
         <small id="groupsHelpBlock" className="form-text text-muted">
         You can assign the project to one group first. Groups can be created and assigned in the <Link to ="/admin/groups">Admin</Link> section.
         </small>
         </div>
       )
     }

    }
    handleChangeGroups(event) {
      const mygroup = event.target.value
      this.setState({groups: mygroup})

    }
    handleChangeUsecase(event) {
        const val = event.target.value
        this.setState({
          use_cases: val
        })
        if(val){
          this.setState({
            data_set_name: this.props.presets[val].data_set_name,
            data_folder: this.props.presets[val].data_folder,
            algo_folder: this.props.presets[val].algo_folder,
            data_file: this.props.presets[val].data_file,
            batch_size: this.props.presets[val].batch_size,
            loss: this.props.presets[val].loss,
            image_width: this.props.presets[val].image_width,
            image_height: this.props.presets[val].image_height,
            image_number_of_channels : this.props.presets[val].image_number_of_channels,
            num_classes: this.props.presets[val].num_classes,
            task_type: this.props.presets[val].task_type,
            accuracy: this.props.presets[val].accuracy
          })
        }

    }

    handleChange(event){
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      //console.log(value)
      //console.log(name)

      this.setState({
        [name]: value
      });


  }

    //render form and button
    render(){
        return(
          <div className="editor-container__inner">

          <form onSubmit={this.handleSubmit} className="container-fluid text-left">
            <div className="editor-container__header" >

              <button type="submit" className="btn btn-success" disabled={this.state.groups === '' ? "disabled" : null}>Submit {this.state.groups === '' ? (<Tippy content="You need to selecta group first!"><span tabIndex="0"><Icon.Info size="18" /></span></Tippy>) : null}</button>
              <button type="cancel" className="btn btn-default" onClick={this.cancel.bind(this)}>Cancel</button>

              {this.props.project ? this.showDates() : null}
              <h2>Create '{this.state.title}' </h2>
            </div>
            <div className="editor-container__content">

                <div className="form-row">
                <div className="col-sm-6">
                  <div className="card bg-light border mb-3">
                  <div className="card-body">
                    <legend className="card-title">Project details</legend>
                    <div className="form-row">
                      <div className="col form-group">
                        <label htmlFor="projectTitle">Title</label>
                        <input value={this.state.title} className="form-control" name="title" onChange={this.handleChange}/>
                      </div>
                      <div className="col form-group">
                          <label htmlFor="projectDescription">Description</label>
                          <input value={this.state.description} className="form-control" name="description" onChange={this.handleChange}/>
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor="groups">Group</label>

                        {this.props.groups && this.props.groups.length > 0 ? this.groups() : <div className="alert alert-warning">
                        You should <Link to ="/admin/groups">create a group</Link> first.
                        </div>}

                    </div>
                    <div className="form-row">
                    <legend>Module selection</legend>
                      <div className="col form-group">
                        <label htmlFor="per_act">Performance </label><br />
                        <Toggle
                          checked={this.state.per_act}
                          name='per_act'
                          onChange={this.handleChange}
                          icons={false} />
                      </div>
                      <div className="col form-group">
                        <label htmlFor="tra_act">Training </label><br />
                        <Toggle
                          checked={this.state.tra_act}
                          name='tra_act'
                          onChange={this.handleChange}
                          icons={false} />
                       </div>
                       <div className="col form-group">
                        <label htmlFor="sec_act">Security</label><br />
                          <Toggle
                            checked={this.state.sec_act}
                            name='sec_act'
                            onChange={this.handleChange}
                            icons={false} />
                      </div>
                      <div className="col form-group">
                        <label htmlFor="rpi_act">Parsimonious</label><br />
                        <Toggle
                          checked={this.state.rpi_act}
                          name='rpi_act'
                          onChange={this.handleChange}
                          icons={false} />
                      </div>
                    </div>


                    </div>
                  </div>

                </div>
                  <div className="col-sm-6">
                  <div className="card">
                    <div className="card-header">Architecture description</div>
                      <div className="card-body">
                          <div className="form-group">

                            <label htmlFor="architecture_name">Architecture name </label>
                            <input type="text" id="architecture_name" className="form-control form-control-sm" onChange={this.handleChange} name="architecture_name" value={this.state.architecture_name}/>
                          </div>
                          <div className="form-group">
                            <label htmlFor="architecture_file">Architecture file</label>
                            <input type="file" className="form-control-file" id="architecture_file" ref={this.fileInput} />
                            <small className="form-text text-muted">Valid format: Json</small>
                          </div>
                          <div className="form-row">
                            <div className="col">
                              <button className="btn btn-block btn-primary btn-sm" onClick={this.uploadArchitecture.bind(this)}>Upload file</button>
                            </div>
                            <div className="col">
                              <input type="text" id="architecture_path" className="form-control form-control-sm" name="architecture_path" disabled={true} value={this.state.architecture_path}/>
                            </div>
                          </div>

                        </div>
                      </div>
                </div>

                <div className="form-row">


                <div className="col-sm-3">
                  <div className="card">
                    <div className="card-header">Constraints</div>
                      <div className="card-body">

                          <div className="form-row">
                          <legend>Power consumption</legend>
                            <div className="col form-group">
                              <label htmlFor="power_value">Value </label>
                              <input type="number" id="power_value" className="form-control form-control-sm" step="0.001" name="power_value" onChange={this.handleChange} value={this.state.power_value}/>
                              <small className="form-text text-muted">[mJ/query]</small>
                            </div>
                            <div className="col form-group">
                            <label htmlFor="power_priority">Priority </label>
                            <select id="power_priority" name="power_priority" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.power_priority}>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            </div>
                          </div>

                          <div className="form-row">
                          <legend>Security</legend>
                            <div className="col form-group">
                              <label htmlFor="security_value">Value </label>
                              <select id="security_value" name="security_value" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.security_value}>
                                  <option value="1">Low</option>
                                  <option value="2">Medium</option>
                                  <option value="3">High</option>
                              </select>
                            </div>
                            <div className="col form-group">
                            <label htmlFor="security_priority">Priority </label>
                            <select id="security_priority" name="security_priority" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.security_priority}>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            </div>
                          </div>

                          <div className="form-row">
                          <legend>Execution time</legend>
                            <div className="col form-group">
                              <label htmlFor="execution_time_value">Value</label>
                              <input type="number" id="execution_time_value" step="0.001" name="execution_time_value" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.execution_time_value}/>
                              <small className="form-text text-muted">[ms/query]</small>

                            </div>
                            <div className="col form-group">
                              <label htmlFor="execution_time_priority">Priority </label>
                              <select id="execution_time_priority"  name="execution_time_priority" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.execution_time_priority}>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                              </select>
                            </div>
                          </div>

                          <div className="form-row">
                          <legend>Accuracy</legend>
                            <div className="col form-group">
                              <label htmlFor="accuracy_value">Time value</label>
                              <input type="number" id="accuracy_value" step="0.01" name="accuracy_value" className="form-control form-control-sm" onChange={this.handleChange}  value={this.state.accuracy_value}/>
                              <small className="form-text text-muted">[%]</small>
                            </div>
                            <div className="col form-group">
                              <label htmlFor="accuracy_priority">Priority </label>
                              <select id="accuracy_priority" onChange={this.handleChange} className="form-control form-control-sm" name="accuracy_priority" value={this.state.execution_time_priority}>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                </select>
                            </div>
                          </div>



                              </div>
                            </div>
                        </div>

                        <div className="col-sm-3">
                          <div className="card">
                            <div className="card-header">Toolflow Settings</div>
                            <div className="card-body">

                            <div className="form-group">
                                <legend>Toolflow presets</legend>
                                {this.props.presets && this.props.presets.length > 0 ? this.presets() : null}
                                <small id="presetsHelpBlock" className="form-text text-muted">
                                Presets can be created in the <Link to ="/admin/presets">Admin</Link> section.
                                </small>
                            </div>

                            <div className="form-group">
                            <legend>Dataset</legend>
                                    <label htmlFor="data_set_name">Dataset Name</label>
                                    <select title="Select a dataset. If you select Custom you have to set a data folder or a data file"
                                    id="data_set_name" name="data_set_name" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.data_set_name}>
                                        <option value="MNIST">MNIST</option>
                                        <option value="CIFAR10">CIFAR10</option>
                                        <option value="CIFAR100">CIFAR100</option>
                                        <option value="CUSTOM">CUSTOM</option>
                                        <option value="test_reply">test_reply</option>
                                        <option value="test_maxq">test_maxq</option>
                                        <option value="test_pke">test_pke</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="data_folder">Dataset folder: </label>
                                    <input  title="This field is required only if you select a custom dataset." className="form-control form-control-sm"  type="text" id="data_folder"
                                           placeholder="Data Folder" name="data_folder" onChange={this.handleChange} value={this.state.data_folder}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="data_file">Dataset file: </label>
                                    <input title="This field is required only if you select a custom dataset. If both file and folder are set, the folder is ignored."
                                    type="text" id="data_file"
                                           placeholder="Data File" name="data_file" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.data_file}/>
                                    <small className="form-text text-muted">The paths should start from the shared data folder</small>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="data_split">Split on train/validation </label>
                                    <input title="Set how to split the dataset between training and validation. The chosen amount goes to training the rest to validation."
                                    type="number" id="data_split" min="0.1" max="0.9" step="0.1" name="data_split" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.data_split}/>

                                    <label htmlFor="num_classes">Number of classes </label>
                                    <input title="Set the number of classes required by your task" type="number" className="form-control form-control-sm" id="num_classes" name="num_classes" onChange={this.handleChange} value={this.state.num_classes}/>
                                </div>


                              </div>
                              </div>
                            </div>
                            <div className="col-sm-3">
                            <div className="card">
                              <div className="card-header">Learning settings</div>
                                <div className="card-body">

                                <div className="form-group">
                                <label htmlFor="algo_folder">Algorithms folder: </label>
                                <input  title="Leave blank to use the automatic generation. Remember to set the gridsearch settings." className="form-control form-control-sm"  type="text" id="algo_folder"
                                      placeholder="Algorithms Folder" name="algo_folder" onChange={this.handleChange} value={this.state.algo_folder}/>
                                </div>

                                <div className="form-row">
                                  <div className="col form-group">
                                    <label htmlFor="task_type">Task type </label>
                                    <select id="task_type" onChange={this.handleChange} name="task_type" className="form-control form-control-sm" value={this.state.task_type}>
                                        <option value="classification">classification</option>
                                        <option value="segmentation">segmentation</option>
                                        <option value="detection">detection</option>
                                    </select>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="mode">Mode</label>
                                    <select id="mode" onChange={this.handleChange} name="mode" className="form-control form-control-sm" value={this.state.mode}>
                                        <option value="full">Full training</option>
                                        <option value="fast">Fast training</option>
                                    </select>
                                  </div>
                                </div>

                                <div className="form-row">
                                  <div className="col form-group">
                                    <label htmlFor="lr">Learning Rate </label>
                                    <input type="number" id="lr" max="0.1" min="0.00001" step="0.00001" name="lr" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.lr}/>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="lr_decay">Decay </label>
                                    <input type="number" id="lr_decay" max="0.1" min="0.00001" step="0.00001" className="form-control form-control-sm" name="lr_decay" onChange={this.handleChange} value={this.state.lr_decay}/>
                                  </div>
                                </div>

                                <div className="form-row">
                                  <div className="col form-group">
                                    <label htmlFor="ref_steps">Iterations</label>
                                    <input type="number" id="ref_steps" name="ref_steps" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.ref_steps} />
                                    <small className="form-text text-muted">How many iterations of learning rate decreasing?</small>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="ref_patience">Epoch wait</label>
                                    <input type="number" id="ref_patience" name="ref_patience" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.ref_patience}/>
                                    <small className="form-text text-muted">How many epochs with unchanged accuracy to wait?</small>
                                  </div>
                                </div>


                                <div className="form-row">
                                  <div className="col form-group">
                                    <label htmlFor="batch_size">Batch size</label>
                                    <input type="number" id="batch_size" name="batch_size" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.batch_size}/>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="num_epochs">Learning epochs</label>
                                    <input type="number" id="num_epochs" name="num_epochs" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.num_epochs}/>
                                  </div>
                                </div>




                                <div className="form-group">
                                    <label htmlFor="loss">Loss function</label>
                                    <select id="loss" onChange={this.handleChange} name="loss" className="form-control form-control-sm" value={this.state.loss}>
                                        <option value="softmax">softmax</option>
                                        <option value="sigmoid">sigmoid</option>
                                        <option value="margin">margin</option>
                                        <option value="detection_loss">detection_loss</option>
                                        <option value="dice">dice</option>
                                    </select>
                                <div className="form-group">
                                </div>
                                    <label htmlFor="optimizer">Optimizer function</label>
                                    <select id="optimizer" onChange={this.handleChange} name="optimizer" className="form-control form-control-sm" value={this.state.optimizer}>
                                        <option value="adam">adam</option>
                                        <option value="amsgrad">amsgrad</option>
                                        <option value="gradient">gradient</option>
                                        <option value="sgd">sgd</option>
                                    </select>
                                </div>

                                <div className="form-row">
                                  <div className="col form-group">
                                    <label htmlFor="nonlin">Nonlinearity </label>
                                    <select id="nonlin" name="nonlin" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.nonlin}>
                                        <option value="relu">relu</option>
                                        <option value="elu">elu</option>
                                        <option value="tanh">tanh</option>
                                    </select>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="accuracy">Accuracy</label>
                                    <select id="accuracy" name="accuracy" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.accuracy}>
                                        <option value="IoU">IoU</option>
                                        <option value="dice">dice</option>
                                        <option value="mse">mse</option>
                                        <option value="percent">percent</option>
                                        <option value="mAP">mAP</option>
                                    </select>
                                  </div>
                                </div>



                              </div>
                            </div>
                          </div>
                          <div className="col-sm-3">

                            <div className="card">
                              <div className="card-header">Images</div>
                                <div className="card-body">

                                <div className="form-row">
                                  <div className="col form-group">
                                    <label htmlFor="image_width">Width </label>
                                    <input type="number" id="image_width" name="image_width" onChange={this.handleChange}  className="form-control form-control-sm"value={this.state.image_width}/>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="image_height">Height </label>
                                    <input type="number" id="image_height" name="image_height" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.image_height}/>
                                  </div>
                                  <div className="col form-group">
                                    <label htmlFor="image_number_of_channels">Channels </label>
                                    <input type="number" id="image_number_of_channels" name="image_number_of_channels" onChange={this.handleChange} className="form-control form-control-sm" value={this.state.image_number_of_channels}/>
                                  </div>
                                </div>


                                <div className="form-group">

                                    <label htmlFor="num_filters">Number of filters in the first layer </label>
                                    <input type="number" id="num_filters" min="16" max="64" step="16" name="num_filters" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.num_filters}/>
                                    <label htmlFor="upconv">Upconvolution function </label>
                                    <select id="upconv" name="upconv" onChange={this.handleChange}  className="form-control form-control-sm" value={this.state.upconv}>
                                        <option value="segmentation">segmentation</option>
                                        <option value="upconv">upconv</option>
                                        <option value="upsampling">upsampling</option>
                                    </select>
                                </div>
                                <div className="form-group">

                                    <div className="form-check">
                                      <input className="form-check-input" type="checkbox" id="augmentation_horizontal" name="augmentation_horizontal" onChange={this.handleChange} value={this.state.augmentation_horizontal} />
                                      <label className="form-check-label" htmlFor="augmentation_horizontal">
                                        Horizontal augmentations
                                      </label>
                                    </div>

                                    <div className="form-check">
                                      <input className="form-check-input" type="checkbox" id="augmentation_vertical" name="augmentation_vertical" onChange={this.handleChange} value={this.state.augmentation_vertical} />
                                      <label className="form-check-label" htmlFor="augmentation_vertical">
                                        Vertical augmentations
                                      </label>
                                    </div>

                                    <div className="form-check">
                                      <input className="form-check-input" type="checkbox" id="normalize" name="normalize" onChange={this.handleChange} value={this.state.normalize} />
                                      <label className="form-check-label" htmlFor="normalize">
                                        Normalization
                                      </label>
                                    </div>

                                    <div className="form-check">
                                      <input className="form-check-input" type="checkbox" id="zero_center" name="zero_center" onChange={this.handleChange} value={this.state.zero_center} />
                                      <label className="form-check-label" htmlFor="zero_center">
                                        Zero centering
                                      </label>
                                    </div>


                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                    </div>

              </div>
              </form>
            </div>
        )
    }
}
function convertOnOff(val){
  switch (val) {
    case 'on':
      return true;
    case 'off':
      return false;
    case true:
      return 'on';
    case false:
      return 'off';
    default:
      return val
  }
}
function mapDispatchToProps(dispatch) {
  return {
    createProject : formData => dispatch(createProject(formData)),
    editProject : (formData, projectId) => dispatch(editProject(formData, projectId)),
    toggleEditor : data => dispatch(toggleEditor(data))
  };
}
export default connect(null, mapDispatchToProps)(ProjectCreator);
