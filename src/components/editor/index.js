import React, { Component} from 'react'
import ProjectCreator from './create'
class Editor extends Component {
  render() {
    return (
      <div id="project-editor" className={this.props.opened ? 'editor-container--opened' : 'editor-container'}>
              {this.props.opened ? (<ProjectCreator project={this.props.project} presets={this.props.presets} groups={this.props.groups} />) : null}
      </div>
    );
    }
}
export default Editor
