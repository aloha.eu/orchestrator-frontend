import React from 'react'

export default class Utils {
  static getColorCode(status){
      switch (status) {
        case 'success':
            return 'success'
        case 'fail':
            return 'warning'
        case 'error':
            return 'danger'
        case 'loading':
            return 'info'
        default:
          return 'primary'
      }
  }
  static toObject(data){
    if(data){
      return JSON.parse(data.replace(/'/g, "\""))
    }else{
      return data
    }
  }
  static toTuples(item){
    return (
      <span>[{item[0]}, {item[1]}]</span>
    )
  }
  static convertOnOff(val){
    switch (val) {
      case 'on':
        return true;
      case 'off':
        return false;
      case true:
        return 'on';
      case false:
        return 'off';
      default:
        return val
    }
  }
}
