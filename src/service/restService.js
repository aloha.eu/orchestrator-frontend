import { ALOHA_FRONTEND_CONFIGURATION } from '../configuration/config.js';
import axios from 'axios';
import FileDownload  from 'js-file-download';

axios.defaults.headers.common['Authorization'] =
                      'Bearer ' + localStorage.getItem('user');

function refreshHeaders(){
  axios.defaults.headers.common['Authorization'] =
                        'Bearer ' + localStorage.getItem('user');
}
export class RestService {

    static async install(){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}start`)
    }
    // lane api
    static async getBoardData(){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/lanes`)
    }

    static async getLaneById(laneId){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/lanes/${laneId}`)
    }

    // card api
    static async updateCard(card){
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/cards/${card.id}`, card)
    }

    static async getCardById(cardId){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/cards/${cardId}`)
    }

    static async getAllCards(){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/cards`)
    }

    static async createCard(card){
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/cards`, card)
    }

    static async deleteCardById(cardId){
        return await axios.delete(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/cards/${cardId}`)
    }
    // project api
    static async createProject(project){
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects`, project)
    }

    static async updateProjectById(project){
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/${project.id}`, project)
    }
    static async getProjects(){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects`)
    }
    static async getProjectsByUser(userId){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/user/${userId}`)
    }
    static async getProjectById(projectId){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/${projectId}`)
    }
    static async getProjectByIdEdit(projectId){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/${projectId}/edit`)
    }

    static async deleteProjectById(projectId){
        return await axios.delete(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/${projectId}`)
    }

    static async getProjectResult(projectId){
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/execution/result/${projectId}`)
    }
    static async uploadFile(data, type){
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/file?type=${type}`, data, { headers: { 'Content-Type': 'multipart/form-data'}})
    }

    static async getProjectFile(filePath){
        return axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/projects/file?filePath=${filePath}`, {responseType: 'blob'}).then((response) => {
            FileDownload(response.data, filePath.substring(filePath.lastIndexOf("/")+1));
        });
    }

    static async openNetron(filePath){ //FIXME: maybe the orchestrator should make the call
        return await axios.post(`http://localhost:5012/api/netron_start?onnx_path=${filePath}`)
    }

    static async startProjectExecution(projectId){
      let headers = {
          'Content-Type': 'application/json',
          'Origin':'*',
          'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
          'Access-Control-Allow-Origin': '*'
        }
        let body = {
            "projectId": projectId
        }
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}/api/projects/execution/start`, {body, headers})
    }
    static async codeProjectExecution(projectId, engine){
      let headers = {
          'Content-Type': 'application/json',
          'Origin':'*',
          'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
          'Access-Control-Allow-Origin': '*'
        }
        let body = {
            "projectId": projectId,
            "engine": engine
        }
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}/api/projects/execution/code`, {body, headers})
    }
    /* PROFILE */
    static async getUser(userId){
      refreshHeaders()
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/users/${userId}`)
    }
    static async getUsers(){
      refreshHeaders()
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/users`)
    }
    static async updateUser(userId, formData){
      refreshHeaders()
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/users/${userId}`, formData)
    }
    static async changePassword(formData){
      refreshHeaders()
        return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/password`, formData)
    }
    /* GROUPS */
    static async getGroups(){
      refreshHeaders()
      return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/groups`)
    }
    static async createGroup(formData){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/groups`, formData)
    }
    static async deleteGroup(groupId){
      refreshHeaders()
      return await axios.delete(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/groups/${groupId}`)
    }
    static async editGroup(groupId, formData){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/groups/${groupId}`, formData)
    }
    static async editGroupRef(groupId, params){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/groups/ref/${groupId}`, params)
    }
    /* PRESETS */
    static async getPresets(){
      refreshHeaders()
        return await axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/presets`)
    }
    static async createPreset(formData){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/presets`, formData)
    }
    static async deletePreset(presetId){
      refreshHeaders()
      return await axios.delete(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/presets/${presetId}`)
    }
    static async editPreset(presetId, formData){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/presets/${presetId}`, formData)
    }
    static async editPresetRef(presetId, params){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/presets/ref/${presetId}`, params)
    }
    /* HASH */
    static async verifyHash(params){
      refreshHeaders()
      return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/hash/verify`, params)
    }
   /* USER */
   static async updateUserAdmin(payload){
     refreshHeaders()
     const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/users/'+payload.usertId
     return await axios.post(url, {[payload.property]: payload.value});
   }
   static async deleteUser(userId){
     refreshHeaders()
     return await axios.delete(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/users/${userId}`)
   }
   static async createUser(formData){
     refreshHeaders()
     return await axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}api/users`, formData)
   }
}
