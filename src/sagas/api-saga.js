import { ALOHA_FRONTEND_CONFIGURATION } from '../configuration/config.js';
import { takeEvery, call, put} from "redux-saga/effects";
import {BOARD_LOADED, BOARD_REQUESTED, UPDATE_CARD, CARD_LOADED, CREATE_PROJECT, PROJECT_CREATED, DELETE_PROJECT, PROJECT_DELETED, START_PROJECTEXECUTION, PROJECT_STATUS, PROJECT_UPDATED, UPDATE_PROJECT, EDIT_PROJECT, PROJECT_EDITED, BOARD_CLEARED} from "../redux/actions/board";
import { AUTHENTICATE_REQUEST, AUTHENTICATED, AUTHENTICATION_ERROR, REGISTER_USER_REQUEST, REGISTER_USER_SUCCESS, REGISTER_USER_FAILURE, VALIDATION_REQUEST, USER_INFO, RECOVER_REQUEST, RECOVER_RESPONSE, RECOVER_PASSWORD, RECOVER_SET} from "../redux/actions/auth";
import axios from 'axios';

export default function* watcherSaga() {
  yield takeEvery(BOARD_REQUESTED, getBoardSaga);
  yield takeEvery(BOARD_CLEARED, clearBoardSaga);
  yield takeEvery(UPDATE_CARD, updateCardSaga);
  yield takeEvery(CREATE_PROJECT, createProjectSaga);
  yield takeEvery(EDIT_PROJECT, editProjectSaga);
  yield takeEvery(DELETE_PROJECT, deleteProjectSaga);
  yield takeEvery(START_PROJECTEXECUTION, startProjectExecution);
  yield takeEvery(UPDATE_PROJECT, updateProjectSaga);
  yield takeEvery(AUTHENTICATE_REQUEST, authenticateUser);
  yield takeEvery(RECOVER_REQUEST, recoverRequest);
  yield takeEvery(RECOVER_PASSWORD, recoverPassword);
  yield takeEvery(REGISTER_USER_REQUEST, registerUser);
  yield takeEvery(VALIDATION_REQUEST, validateUser)
}

function* getBoardSaga(action) {
    const payload = {};
    payload.lanes = yield call(getData);
    payload.presets = yield call(getPresetsByUser, action.payload.userId);
    payload.projects = yield call(getProjectsByUser, action.payload.userId);
    payload.users = yield call(getUsers);
    yield put({ type: BOARD_LOADED, payload});
}
function* clearBoardSaga(action) {
    const payload = {};
    payload.lanes = yield call(getData);
    payload.presets = [];
    payload.projects = [];
    payload.users = [];
    yield put({ type: BOARD_LOADED, payload});
}
/*function getProjectsData(){
  const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/projects';
  return fetch(url).then(response =>
    response.json()
  );
}*/
function getProjectsByUser(userId){
  const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/projects/user/'+userId;
  return fetch(url).then(response =>
    response.json()
  );
}
function getPresetsByUser(userId){
  const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/presets/user/'+userId;
  return fetch(url).then(response =>
    response.json()
  );
}
function getUsers(){
  const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/users';
  return fetch(url).then(response =>
    response.json()
  );
}

function getData() {
  const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/lanes'
  return fetch(url).then(response =>
    response.json()
  );
}

function* updateCardSaga(action) {
    const data = action.payload;
    console.log(action)
    data.cardDetails.laneId = data.targetLaneId;
    const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/cards/'+data.cardDetails.id
    const payload = yield axios.post(url, data.cardDetails);
    yield put({ type: CARD_LOADED, payload, data});
}

function* createProjectSaga(action){
try{
    console.log('create project saga')
    const data = action.payload;
    const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/projects'
    const payload = yield axios.post(url, data);
    yield put({ type: PROJECT_CREATED, payload});
  }catch(err){
    console.log(err)
  }
}

function* editProjectSaga(action){
try{
    console.log('edit project saga')
    const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/projects/'+action.projectId+'/edit'
    const payload = yield axios.post(url, action.formData);
    yield put({ type: PROJECT_EDITED, payload});
  }catch(err){
    console.log(err)
  }
}

function* deleteProjectSaga(action){
  try{
    const data = action.payload;
    const urlProject = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/projects/'+data.projectId
    const urlCard = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/cards/'+data.cardId
    yield axios.delete(urlProject)
    yield axios.delete(urlCard)
    yield put({ type: PROJECT_DELETED, data});
  }catch(err){
    console.log(err)
  }
}

function* startProjectExecution(action){
  try{
    const data = action.payload;
    data.cardDetails.label = data.newStatus
    const projectInfo = {  projectId: data.projectId, cardDetails: data.cardDetails}
    const urlProject = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'/api/projects/execution/start'
    const payload = yield axios.post(urlProject, projectInfo);
    //console.log(payload)
    //const urlCard = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/cards/'+data.cardDetails.id
    //const dataref = yield axios.post(urlCard, data.cardDetails);
    //console.log(dataref);
    yield put({ type: PROJECT_STATUS, payload});
  }catch(err){
    console.log(err)
  }
}
function* updateProjectSaga(action){
  const payload = action.payload
  console.log(payload)
  const url = ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']+'api/projects/'+payload.projectId
  //const response = yield axios.post(url, {[payload.property]: payload.value});
  yield axios.post(url, {[payload.property]: payload.value});
  yield put({ type: PROJECT_UPDATED, payload});
}
function* authenticateUser(action){
  console.log('--> authenticating')
  try{
    const resp  = yield axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/login`, { email: action.payload.email, password: action.payload.password })
    localStorage.setItem('user', resp.data.auth_token);
    yield put({type: USER_INFO, payload: resp});
    yield  put({ type: AUTHENTICATED, payload: resp });
  }catch(error){
    console.log(error.response.data.message)
    yield put({  type: AUTHENTICATION_ERROR, error: error.response.data });
  }
}
function* recoverPassword(action){
  console.log('--> Generating recover email')
  const resp  = yield axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/hash/set`,  action.payload)
  yield  put({ type: RECOVER_RESPONSE, payload: resp });
}
function* recoverRequest(action){
  console.log('--> Saving new password')
  const resp  = yield axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/hash`,  action.payload)
  yield  put({ type: RECOVER_SET, payload: resp });
}
function* registerUser(action){
  console.log('--> registering user')
  try{
    const resp  = yield axios.post(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/register`, action.payload)
    localStorage.setItem('user', resp.data.auth_token);
    yield  put({ type: REGISTER_USER_SUCCESS, payload: resp });
  }catch(error){
    console.log(error.response.data.message)
  yield put({  type: REGISTER_USER_FAILURE, error: error.response.data });
  }
}
function* validateUser(action){
  console.log('--> validating user token')
  try{
    const resp  = yield axios.get(`${ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']}auth/status`,  { headers: { Authorization: `Bearer ${action.user}` } })
    console.log(resp.status)
    yield put({type: USER_INFO, payload: resp});
    yield put({ type: AUTHENTICATED, payload: resp });
  }catch(error){
    console.log(error.response.data.message)
    yield put({  type: AUTHENTICATION_ERROR, error: error.response.data });
  }
}
