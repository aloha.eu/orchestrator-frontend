import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import rootReducer from '../reducers/board';
import { reducer as formReducer } from 'redux-form';
import authReducer from '../reducers/auth';
import createSagaMiddleware from "redux-saga";
import apiSaga from "../../sagas/api-saga";

const sagaMiddleware = createSagaMiddleware({
  onError: (payload) => {
    console.log('An error ocurred:')
    console.log(payload)
    store.dispatch({ type: 'API_ERRORED', payload })
  }
});

const storeEnhancers =  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      'trace': true
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const allReducers = combineReducers ({
board: rootReducer,
form: formReducer,
auth: authReducer

});


const store = createStore(allReducers,
  storeEnhancers(applyMiddleware(sagaMiddleware))
  )

sagaMiddleware.run(apiSaga);

export default store
