import { AUTHENTICATED, UNAUTHENTICATED, AUTHENTICATION_ERROR, REGISTER_USER_SUCCESS, REGISTER_USER_FAILURE, USER_INFO, RECOVER_RESPONSE, RECOVER_SET} from '../actions/auth';
import update from 'immutability-helper'

//import history from "../../history";

const initialState = {
    authenticated: false,
    is_authenticating: false,
    user: {},
    error: ""
}
export default function(state=initialState, action) {
  switch(action.type) {
    case AUTHENTICATED:
      console.log('--> user authenticated')
      return { ...state, authenticated: true};
    case USER_INFO:
      return update(state, {user: {$set: action.payload.data.data}})
      //history.push('/board');
    case UNAUTHENTICATED:
      console.log('--> user logged out')
      return { ...state,  error: 'Please, login again.', authenticated: false, user: {} };
      //history.push('/login');
    case AUTHENTICATION_ERROR:
      console.log('--> error authenticating')
      localStorage.clear();
      console.log(action.error);
      return { ...state, error: action.error.message };
    case REGISTER_USER_SUCCESS:
      console.log('--> user registered')
      return { ...state, authenticated: true };
      //history.push('/board');
    case REGISTER_USER_FAILURE:
      console.log('--> error registering')
      return { ...state, error: action.error.message };
    case RECOVER_RESPONSE:
      console.log('--> recover email sent')
      return { ...state, error: action.payload.data.message };
    case RECOVER_SET:
      console.log('--> password saved')
      return { ...state, error: action.payload.data.message };
    default:
    return state;
  }
}
