import LaneHelper from '../LaneHelper'
import { store } from 'react-notifications-component';
import update from 'immutability-helper'

const initialState = {
    data: {lanes: []},
    projects: [],
    presets: [],
    eventBus: undefined,
    popUpOpened: false,
    editor: {
      opened: false,
      project: null
    },
    message: '',
    error: false,
    initialLane: undefined,
    currentGroup: null,
    loading: true,
};

export default function dataReducer(state = initialState, action) {
  switch (action.type) {
    case 'LOADING_BOARD':
    return Object.assign({}, state, {
      loading: action.payload
    })
    case 'SET_EVENTBUS':
    return Object.assign({}, state, {
      eventBus: action.payload
    })
    case 'POPUP_OPEN':
      return Object.assign({}, state, {
        popUpOpened: action.payload.popUpOpened
      })
      case 'SET_INITIALLANE':
        return Object.assign({}, state, {
          setInitialLane: action.payload.setInitialLane
        })
    case 'BOARD_LOADED':
      let lanes = action.payload.lanes;
      lanes=Object
      .entries(lanes)
      .sort()
      .reduce((obj, [k,v]) => ({
        ...obj,
        [k]: v
      }), {})
      return Object.assign({}, state, {
        data: {lanes: action.payload.lanes},
        initialLane: Object.values(lanes)[0].id,
        projects: action.payload.projects,
        presets: action.payload.presets,
        users: action.payload.users,
        loading: false
      })

    case 'CARD_LOADED':
    return LaneHelper.moveCardAcrossLanes(state,{
              fromLaneId: action.data.sourceLaneId,
              toLaneId: action.data.targetLaneId,
              cardId: action.data.cardId,
              index: action.data.position
            })
    /*  return Object.assign({}, state, {
        message: "card loaded"
      })*/
    case 'UPDATE_BOARD':
      return Object.assign({}, state, {
        data: {lanes: action.payload}
      })
    case 'PROJECT_CREATED':
      console.log("Project created, will receive websocket from server.")
      return Object.assign({}, state, {
          message: "project created"
        })
    case 'PROJECT_DELETED':
      return LaneHelper.removeCardFromLane(state,{
                laneId: action.data.laneId,
                cardId: action.data.cardId
              })
    case 'PROJECT_STATUS':
      console.log('status message')
      console.log(action)
      let status_msg = "";
      if(action.payload.data.message === "WIP"){
        status_msg = action.payload.data.message
      }else{
        status_msg = action.payload.data.jobId+' is '+action.payload.data.status
      }
          store.addNotification({
              title: "Project status",
              message: status_msg,
              type: "warning",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 5000,
                onScreen: true
              }
            });
      return state;
    case "PROJECT_UPDATED":
      const proj = state.projects.find(proj => proj.id === action.payload.projectId)
      const newstate = state.projects.map(project => {
            if(project.id === action.payload.projectId){
              let p = action.payload.property
              if (p === 'tra_en' || p === 'sec_en' || p === 'rpi_en' || p === 'per_en'){
                  return update(proj, {[p]: { $set: {active: action.payload.value, status: action.payload.value === true ? "Ready" : "Disabled"} } })
                }else{
                  return update(proj, {[action.payload.property]: {$set: action.payload.value}})
                }

            }else{return project}
          })
      return update(state,{projects: {$set: newstate}});
    // --- API ERRORS -- //
    case 'API_ERRORED': //ERROR HANDLING FROM SAGAS MIDLEWARE - check how to manage!
      console.log(action)
      let msg =  action.payload.message === null ? action.payload : action.payload.message;
      store.addNotification({
        title: "API Error",
        message: "A critical error ocurred or the backend is down. Please reload the page.",
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 10000,
          onScreen: true
        }
      });

      return Object.assign({}, state, {
        message: msg,
        error: true
      })
    case 'TOGGLE_EDITOR':
    //  console.log(action)
      return update(state,{editor: {$set: action.payload}});
    default:
      return state;
  }
};
