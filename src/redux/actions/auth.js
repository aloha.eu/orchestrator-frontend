export const AUTHENTICATE_VALIDATE = 'AUTHENTICATE_VALIDATE';
export const AUTHENTICATED = 'AUTHENTICATED';
export const UNAUTHENTICATED = 'UNAUTHENTICATED';
export const AUTHENTICATION_ERROR = 'AUTHENTICATION_ERROR';
export const AUTHENTICATE_REQUEST = 'AUTHENTICATE_REQUEST';
export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';
export const VALIDATION_REQUEST = 'VALIDATION_REQUEST';
export const USER_INFO = 'USER_INFO';
export const RECOVER_REQUEST = 'RECOVER_REQUEST';
export const RECOVER_RESPONSE = 'RECOVER_RESPONSE';
export const RECOVER_PASSWORD = 'RECOVER_PASSWORD';
export const RECOVER_SET = 'RECOVER_SET';

export function signInAction(payload) {
  // { email, password }, history
  return { type: AUTHENTICATE_REQUEST, payload };
}
export function recoverAction(payload) {
  // { email, password }, history
  return { type: RECOVER_REQUEST, payload };
}
export function recoverPass(payload) {
  // { email, password }, history
  return { type: RECOVER_PASSWORD, payload };
}
export function validateUser(payload) {
  // { email, password }, history
  return { type: VALIDATION_REQUEST, payload };
}
export function signOutAction() {
  console.log('--> Attempt to logout')
  localStorage.clear();
  return {
    type: UNAUTHENTICATED
  };
}
export function registerUser(payload){
  return { type: REGISTER_USER_REQUEST, payload };
}
