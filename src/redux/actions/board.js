export const SET_EVENTBUS = "SET_EVENTBUS";
export const POPUP_OPEN = "POPUP_OPEN";
export const BOARD_LOADED = "BOARD_LOADED";
export const BOARD_REQUESTED = "BOARD_REQUESTED";
export const BOARD_CLEARED = "BOARD_CLEARED";
export const API_ERRORED = "API_ERRORED";
export const UPDATE_CARD = "UPDATE_CARD";
export const CARD_LOADED = "CARD_LOADED";
export const UPDATE_BOARD = "UPDATE_BOARD";
export const CREATE_PROJECT = "CREATE_PROJECT";
export const PROJECT_CREATED = "PROJECT_CREATED";
export const EDIT_PROJECT = "EDIT_PROJECT";
export const PROJECT_EDITED = "PROJECT_EDITED";
export const DELETE_PROJECT = "DELETE_PROJECT";
export const PROJECT_DELETED = "PROJECT_DELETED";
export const SET_INITIALLANE = "SET_INITIALLANE";
export const START_PROJECTEXECUTION = "START_PROJECTEXECUTION";
export const PROJECT_STATUS = "PROJECT_STATUS";
export const UPDATE_PROJECT = "UPDATE_PROJECT";
export const PROJECT_UPDATED = "PROJECT_UPDATED";
export const TOGGLE_EDITOR = 'TOGGLE_EDITOR';
export const LOADING_BOARD = 'LOADING_BOARD';

export function loadingBoard (payload){
  return { type: LOADING_BOARD, payload };
}
export function setEventBus(payload) {
  return { type: SET_EVENTBUS, payload };
}
export function setInitialLane(payload) {
  return { type: SET_INITIALLANE, payload };
}
export function popUpOpen(payload) {
  return { type: POPUP_OPEN, payload };
}

export function clearData() { // gets board from server
  return { type: BOARD_CLEARED};
}
export function getBoardData(payload) { // gets board from server
  return { type: BOARD_REQUESTED, payload};
}
export function updateCard(payload){ // sends card change to server
  return { type: UPDATE_CARD, payload};
}

export function updateBoardData(payload){
  return { type: UPDATE_BOARD, payload};
}

export function createProject(payload){
  return { type: CREATE_PROJECT, payload};
}
export function editProject(formData, projectId){
  return { type: EDIT_PROJECT, formData, projectId};
}
export function deleteProject(payload){
  return {type: DELETE_PROJECT, payload}
}

export function startProjectExecution(payload){
  return {type: START_PROJECTEXECUTION, payload}
}
export function updateProject(payload){
  return {type: UPDATE_PROJECT, payload}
}
export function toggleEditor(payload){
  return {type: TOGGLE_EDITOR, payload}
}
