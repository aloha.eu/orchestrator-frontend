import React, { Component } from 'react';
import { Router, Route, Link, useLocation} from 'react-router-dom';
import { Provider} from 'react-redux'
import store from './redux/store'
import ReactNotification from 'react-notifications-component'
import {store as notification_store } from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import 'tippy.js/animations/perspective.css';

import { VALIDATION_REQUEST } from './redux/actions/auth';
import { getBoardData, toggleEditor} from './redux/actions/board';
import history from "./history";
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper'
import connectedAuthWrapper from 'redux-auth-wrapper/connectedAuthWrapper'


// IMPORT COMPONENTS
import MyBoard from "./components/board";
import ErrorBoundary from "./components/ErrorBoundary";
import Homepage from './components/home';
import Install from './components/install';
import Login from './components/auth/login';
import Register from './components/auth/register';
import Profile from './components/profile';
import Navbar from './components/navbar';
import Tutorial from './components/tutorial';
import AdminMain from './components/admin';
import Recover from './components/auth/recover';

import { ALOHA_FRONTEND_CONFIGURATION } from './configuration/config.js';
import io from 'socket.io-client';

import {PlusSquare} from 'react-feather'
const AlohaLogo = require('./assets/images/aloha-logo.png');


/* ---- SOCKET CONNECTIONS ---- */
const socket = io(ALOHA_FRONTEND_CONFIGURATION['orchestratorAPIUrl']);

    socket.once('connect', (msg) => {
        console.log("Websocket connection open");
    });
    socket.once('connect_error', (error) => {
        console.log(error);
        notification_store.addNotification({
          title: "SOCKET CONNECTION ERROR",
          message: "There was a problem connecting with the server. Please make sure backend is running and reload.",
          type: "danger",
          insert: "top",
          container: "top-right",

        });
    });

    socket.on('new_project', (msg) => {
        console.log("new_project received");
        //console.log(msg)
        notification_store.addNotification({
          title: "Project Info",
          message: "A project has been created",
          type: "success",
          insert: "top",
          container: "top-right",
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });
        store.dispatch(getBoardData({userId: store.getState().auth.user.user_id}))

    });
    socket.on('edit_project', (msg) => {
        console.log("edit_project received");
        const proj = store.getState().board.projects.find(p => p.id === msg)
        if(proj){
          notification_store.addNotification({
            title: "Project Info",
            message: "The project has been updated",
            type: "success",
            insert: "top",
            container: "top-right",
            dismiss: {
              duration: 5000,
              onScreen: true
            }
          });
          store.dispatch(getBoardData({userId: store.getState().auth.user.user_id}))
        }

    });
    socket.on('card_status', (msg) => {
        console.log("card_status received");
        console.log(msg)
        store.dispatch(getBoardData({userId: store.getState().auth.user.user_id}))
    });

    socket.on('card_deleted', (msg) => {
        console.log(msg)
        console.log("card deleted received");
        notification_store.addNotification({
          title: "Card Info",
          message: "A card has beend deleted",
          type: "info",
          insert: "top",
          container: "top-right",
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });
        store.dispatch(getBoardData({userId: store.getState().auth.user.user_id}))
    });

    socket.on('user_status', (msg) => {
        console.log("user_status received");
        const user = localStorage.getItem('user');
        if(user) {
          store.dispatch({ type: VALIDATION_REQUEST, user });
        }else{
          console.log("No user defined. Please log in.");
        }
    });
    //if card status is changed, make update to board
    socket.on('status',(msg) => {
      //  console.log("status received, message:" );
      //  console.log(msg);
        let status_msg;
      //  console.log(msg.cardId)
        if(msg.cardId !== undefined){
      //    console.log('msg cardId')
          status_msg = msg.cardId+' changed to '+msg.status
        }else{
      //    console.log('msg title')
          status_msg = msg.title+' changed to '+msg.label
        }

        const proj = store.getState().board.projects.find(p => p.id === msg.jobId)
        if(proj){
          notification_store.addNotification({
            title: "Process status",
            message: status_msg,
            type: "success",
            insert: "top",
            container: "top-right",
            dismiss: {
              duration: 5000,
              onScreen: true
            }
          });
          store.dispatch(getBoardData({userId: store.getState().auth.user.user_id}))
        }
    });


    //if process is started
    socket.on('project_executed',(msg) => {

        console.log("project_executed received, message:" );
        console.log(msg);
        notification_store.addNotification({
          title: "Process info",
          message: "A process have started",
          type: "success",
          insert: "top",
          container: "top-right",
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });

      store.dispatch(getBoardData({userId: store.getState().auth.user.user_id}))
    });
/* ---- AUTHENTICATION ROUTING ---- */
  const userIsAuthenticated = connectedRouterRedirect({
     // The url to redirect user to if they fail
    redirectPath: '/login',
     // If selector is true, wrapper will not redirect
     // For example let's check that state contains user data
    authenticatedSelector: state => state.auth.authenticated === true,
    // A nice display name for this check
    wrapperDisplayName: 'UserIsAuthenticated'
  })
const visibleIsAuthenticated = (Component) => connectedAuthWrapper({
  authenticatedSelector: state => state.auth.authenticated === true,
  wrapperDisplayName: 'visibleIsAuthenticated',
})(Component)
const locationHelper = locationHelperBuilder({})
const userIsNotAuthenticated = connectedRouterRedirect({
  // This sends the user either to the query param route if we have one, or to the landing page if none is specified and the user is already logged in
  redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/board',
  // This prevents us from adding the query parameter when we send the user away from the login page
  allowRedirectBack: false,
   // If selector is true, wrapper will not redirect
   // So if there is no user data, then we show the page
  authenticatedSelector: state => state.auth.authenticated === false,
  // A nice display name for this check
  wrapperDisplayName: 'UserIsNotAuthenticated'
})
//const ProjectCreatorLink = visibleIsAuthenticated(() => <ProjectCreator />)
const ProjectEditorLink = visibleIsAuthenticated(function(){
  let location = useLocation();
  if(location.pathname === '/board'){
    return(
      <li className="nav-item">
      <button className={store.getState().board.editor.opened === false ? "nav-link btn btn-sm btn-primary" : "active btn btn-sm btn-secondary" } onClick={() => store.dispatch(toggleEditor({ opened: store.getState().board.editor.opened === true ? false : true, project: null}))}><PlusSquare size="15" /> New Project</button>
      </li>
    )
  }else{
    return null
  }

})
class App extends Component {
    componentDidMount(){
      const user = localStorage.getItem('user');
      if(user) {
        store.dispatch({ type: VALIDATION_REQUEST, user });
      }
    }
    //render whole app
    render() {
        return (
          <Provider store={store}>
            <Router history={history}>
                <div className="App">
                <ErrorBoundary>
                <ReactNotification />
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                      <Link to="/" className="navbar-brand" >
                        <img src={AlohaLogo} height="30" className="d-inline-block align-top" alt="" />
                          Toolflow <span className="navbar-text badge badge-light">v{ALOHA_FRONTEND_CONFIGURATION['version']}</span>
                      </Link>
                      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                      </button>
                        <Navbar />
                        <ul className="nav">
                            <ProjectEditorLink />
                        </ul>

                    </nav>
                    <Route exact path="/" component={userIsNotAuthenticated(Homepage)} />
                    <Route exact path="/install" component={userIsNotAuthenticated(Install)} />
                    <Route path="/login" component={userIsNotAuthenticated(Login)} />
                    <Route path="/register" component={userIsNotAuthenticated(Register)} />
                    <Route path="/board" component={userIsAuthenticated(MyBoard)} />
                    <Route path="/tutorial" component={Tutorial} />
                    <Route path="/profile" component={userIsAuthenticated(Profile)} />
                    <Route path="/admin" component={userIsAuthenticated(AdminMain)} />
                    <Route path="/recover/:hash?" component={Recover} />
                </ErrorBoundary>
                </div>
              </Router>
          </Provider>

        );
    }
}
export default App;
